<?php

class NcseHelper extends MvcHelper {

  public function admin_table_cells($controller, $objects, $options = array()) {
      $html = '';
      foreach ($objects as $object) {
          $html .= '<tr>';
          foreach ($controller->default_columns as $key => $column) {
              $html .= $this->admin_table_cell($controller, $object, $column, $options);
          }
          $html .= $this->admin_actions_cell($controller, $object, $options);
          $html .= '</tr>';
      }
      return $html;
  }

  public function admin_table_cell($controller, $object, $column, $options = array()) {
      if (!empty($column['value_method'])) {
          $value = $controller->{$column['value_method']}($object);
      } else {
          $value = $object->{$column['key']};
          if ($column['key']=='date') {
            $value = mysql2date( 'm-d-Y', $object->{$column['key']} );
          }
          if ($column['key']=='time') {
            $value = mysql2date( 'g:i a', $object->{$column['key']} );
          }
          if ($column['key']=='availability') {
            $value = $object->{$column['key']};
            if ($value=='inactive') {
              $value = '&#x2717';
            }else{
              $value = '&#x2713;';
            }
          }
          if ($column['key']=='inactive') {
            $value = $object->{$column['key']};
            if ($value) {
              $value = '&#x2717';
            }else{
              $value = '&#x2713;';
            }
          }
          if (is_array($value)) {
            if ($controller->model_name=='Event') {
              $newValue='';
              foreach ($value as $vkey => $vvalue) {
                $newValue .=  mysql2date( 'm-d-Y', $vvalue->date ) . ' - ' . $vvalue->venue->name . '<br/>';
              }
              $value = $newValue;
            }
          }elseif (is_object($value)) {
            if ($controller->model_name == 'EventDate') {
              $value = $value->name;
            }

          }
      }
      return '<td>'.$value.'</td>';
  }

  public function admin_actions_cell($controller, $object, $options = array()) {

      $default = array(
          'actions' => array(
              'edit' => true,
              'view' => true,
              'delete' => true,
          )
      );

      $options = array_merge($default, $options);

      $links = array();
      $object_name = empty($object->__name) ? 'Item #'.$object->__id : $object->__name;
      $encoded_object_name = $this->esc_attr($object_name);

      if($options['actions']['edit']){
          $links[] = '<a href="'.MvcRouter::admin_url(array('object' => $object, 'action' => 'edit')).'" title="Edit '.$encoded_object_name.'">Edit</a>';
      }

      if($options['actions']['view']){
          $links[] = '<a href="'.MvcRouter::public_url(array('object' => $object)).'" title="View '.$encoded_object_name.'">View</a>';
      }

      if($options['actions']['delete']){
          $links[] = '<a href="'.MvcRouter::admin_url(array('object' => $object, 'action' => 'delete')).'" title="Delete '.$encoded_object_name.'" onclick="return confirm(&#039;Are you sure you want to delete '.$encoded_object_name.'?&#039;);">Delete</a>';
      }

      $html = implode(' | ', $links);
      return '<td>'.$html.'</td>';
  }

}

?>
