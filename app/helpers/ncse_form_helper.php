<?php

class NcseFormHelper extends MvcFormHelper {

  public function construct($model_name, $options=array()){
    $this->model_name = $model_name;
    $this->object = MvcObjectRegistry::get_object($model_name);
  }

  public function table_row_data($name,$value){
    $html = '<tr><td>'.$name.'</td><td>'.$value.'</td></tr>';
    return $html;
  }

  public function wysiwyg_editor($title,$value,$field_name,$table=true,$media_button=false){
    if ($table) {
      echo '<tr><td>'.$title.'</td>';
      echo '<td>';
      wp_editor( $value, str_replace(' ','_',strtolower($title)), array('media_buttons'=>$media_button,'editor_height'=>'200','textarea_name'=>$this->input_name($field_name)) );
      echo '</td></tr>';
    }else{
      wp_editor( $value, str_replace(' ','_',strtolower($title)), array('media_buttons'=>$media_button,'editor_height'=>'200','textarea_name'=>$field_name) );
    }
  }

  public function image_uploader($field_name,$value,$buttonTxt="Upload Image"){
    return '<input type="text" name="'.$this->input_name($field_name).'" class="regular-text image_url" value="'.$value.'">
    <input type="button" name="upload-btn" class="button-secondary upload-btn" value="'.$buttonTxt.'">';
  }

  public function date_picker($field_name){
      $html = '<input class="datepicker" type="text" name="'.$this->input_name($field_name).'" value="'.$this->object->$field_name.'">';
      return $html;
  }

  public function venue_select2_options($city_and_venue,$selected_value){
    $venueOptionsHtml = '';
    foreach ($city_and_venue as $city => $venues) {
      $venueOptionsHtml .= '<optgroup label="'.$city.'">';
      foreach ($venues as $venue) {
        $selected = '';
        if (!empty($selected_value)) {
          if ($selected_value == $venue['id']) {
            $selected = ' selected';
          }
        }
        $venueOptionsHtml .= '<option value="'.$venue['id'].'"'.$selected.'>'.$venue['name'].'</option>';
      }
      $venueOptionsHtml .= '</optgroup>';
    }
    return $venueOptionsHtml;
  }

  public function artist_select2_options($artists){
    $selected_value = array();
    // if (!empty($selected_artists)) {
    //   foreach ($selected_artists as $artist) {
    //     array_push($selected_value,$artist->id);
    //   }
    // }
    $artistOptionsHtml = '';
    foreach ($artists as $artist) {
      $selected = '';
      // if (!empty($selected_value)) {
      //   if (in_array($artist->id,$selected_value)) {
      //     $selected = ' selected';
      //   }
      // }
      $artistOptionsHtml .= '<option value="'.$artist->id.'"'.$selected.'>'.$artist->name.'</option>';
    }
    return $artistOptionsHtml;
  }

  public function artist_container($selected_artists,$artists_order,$input_name){
    $artists = '';
    $count = 0;

    if (!$artists_order && $selected_artists) {
      foreach ($selected_artists as $artist) {
        $artists .= '<li data-id="'.$artist->id.'"><span class="drag"><i class="fa fa-arrows" aria-hidden="true"></i></span>'.$artist->name.'<input type="hidden" data-num="'.$count.'" name="'.$input_name.'['.$count.']" value="'.$artist->id.'"/><span class="remove-artist">X</span></li>';
        $count++;
      }
    }else{
      $order = explode(',',$artists_order);
      $artistsArray = array();
      foreach ($selected_artists as $artist) {
        $artistsArray[$artist->id] = $artist;
      }
      foreach ($order as $key => $value) {
        $artist = $artistsArray[$value];
        $artists .= '<li data-id="'.$artist->id.'"><span class="drag"><i class="fa fa-arrows" aria-hidden="true"></i></span>'.$artist->name.'<input type="hidden" data-num="'.$count.'" name="'.$input_name.'['.$count.']" value="'.$artist->id.'"/><span class="remove-artist">X</span></li>';
        $count++;
      }
    }
    return $artists;
  }

  public function series_event_dates_select_options($events,$selected_value){
    $seriesEventDatesOptionsHtml = '';
    foreach ($events as $event) {
      if ($event->event_dates && count($event->event_dates)>0) {
        foreach ($event->event_dates as $event_date) {
          $selected = '';
          if (!empty($selected_value)) {
            if ($selected_value == $event_date->id) {
              $selected = ' selected';
            }
          }
          $seriesEventDatesOptionsHtml .= '<option value="'.$event_date->id.'"'.$selected.'>'.$event->name.' - '.mysql2date( 'l, F j, Y', $event_date->date).' | '.mysql2date( 'g:i a', $event_date->time).' | '.$event_date->venue->name.'</option>';
        }
      }
    }
    return $seriesEventDatesOptionsHtml;
  }

  public function load_image_uploader_script(){
    echo '<script type="text/javascript">
      jQuery(document).ready(function($){
        $(".upload-btn").click(function(e) {
          e.preventDefault();
          var that = $(this);
          var image = wp.media({
            title: "Upload Image",
            multiple: false
          }).open().on("select", function(e){
            var uploaded_image = image.state().get("selection").first();
            var image_url = uploaded_image.toJSON().url;
            that.parent().find(".image_url").val(image_url)
          });
        });
      });
    </script>';
  }

  public function verbose_date_range($start_date = '',$end_date = '') {

      $date_range = '';

      // If only one date, or dates are the same set to FULL verbose date
      if ( empty($start_date) || empty($end_date) || ( mysql2date('FjY',$start_date) == mysql2date('FjY',$end_date) ) ) { // FjY == accounts for same day, different time
          $start_date_pretty = mysql2date( 'F j, Y', $start_date );
          $end_date_pretty = mysql2date( 'F j, Y', $end_date );
      } else {
           // Setup basic dates
          $start_date_pretty = mysql2date( 'F j', $start_date );
          $end_date_pretty = mysql2date( 'j, Y', $end_date );
          // If years differ add suffix and year to start_date
          if ( mysql2date('Y',$start_date) != mysql2date('Y',$end_date) ) {
              $start_date_pretty .= mysql2date( ', Y', $start_date );
          }

          // If months differ add suffix and year to end_date
          if ( mysql2date('F',$start_date) != mysql2date('F',$end_date) ) {
              $end_date_pretty = mysql2date( 'F ', $end_date) . $end_date_pretty;
          }
      }

      // build date_range return string
      if( ! empty( $start_date ) ) {
            $date_range .= $start_date_pretty;
      }

      // check if there is an end date and append if not identical
      if( ! empty( $end_date ) ) {
          if( $end_date_pretty != $start_date_pretty ) {
                $date_range .= ' - ' . $end_date_pretty;
            }
       }
      return $date_range;
  }


  private function input_name($field_name) {
      return 'data['.$this->model_name.']['.MvcInflector::underscore($field_name).']';
  }

  public function belongs_to_dropdown($model_name, $select_options, $options=array()) {

      if (!empty($this->model->associations[$model_name])) {
          $foreign_key = $this->model->associations[$model_name]['foreign_key'];
      } else {
          $foreign_key = MvcInflector::underscore($model_name).'_id';
      }

      $value = empty($this->object->{$foreign_key}) ? '' : $this->object->{$foreign_key};

      $defaults = array(
          'id' => $this->model_name.'_'.$model_name.'_select',
          'name' => 'data['.$this->model_name.']['.$foreign_key.']',
          'label' => MvcInflector::titleize($model_name),
          'value' => $value,
          'options' => $select_options,
          'empty' => true
      );
      $options = array_merge($defaults, $options);
      $select_options = $options;
      $select_options['label'] = null;
      $select_options['before'] = null;
      $select_options['after'] = null;

      $field_name = $options['name'];

      $html = $this->before_input($field_name, $options);
      $html .= $this->select_tag($field_name, $select_options);
      $html .= $this->after_input($field_name, $options);

      return $html;
  }

  public function select_tag($field_name, $options=array()) {
      $defaults = array(
          'empty' => false,
          'value' => null
      );

      $options = array_merge($defaults, $options);
      $options['options'] = empty($options['options']) ? array() : $options['options'];
      $options['name'] = $field_name;
      $attributes_html = self::attributes_html($options, 'select');
      $html = '<select'.$attributes_html.'>';
      if ($options['empty']) {
          $empty_name = is_string($options['empty']) ? $options['empty'] : '';
          $html .= '<option value="">'.$empty_name.'</option>';
      }
      foreach ($options['options'] as $key => $value) {
          if (is_object($value)) {
              $key = $value->__id;
              $value = $value->__name;
          }
          $selected_attribute = $options['value'] == $key ? ' selected="selected"' : '';
          $html .= '<option value="'.$this->esc_attr($key).'"'.$selected_attribute.'>'.$value.'</option>';
      }
      $html .= '</select>';
      return $html;
  }

  public function display_action_buttons($params){
    $html = '<div><table><tr><td>
            <a href="'.MvcRouter::admin_url(array('controller'=>$params['controller'],'action'=>'index')).'" class="button">Go back to index</a></div>
          </td>';
    if ($params['action']=='edit') {
      $html .= '<td><a href="'.MvcRouter::admin_url(array('controller'=>$params['controller'],'action'=>'add')).'" class="button">Add New</a></div></td>';
    }
    $html .= '</tr></table><br/></div>';
    return $html;
  }

  public function get_city_and_state($city){
    $city_state = $city->name;
    if ($city->state && !empty($city->state)) {
      $city_state .= ', ' . $this->get_state_acronym($city->state);
    }
    return $city_state;
  }

  public function get_state_acronym($state){
    $state_acronym = '';
    if (!empty($state)) {
      $state_array = explode(' ',$state);
      foreach ($state_array as $key => $value) {
        $state_acronym .= $value[0];
      }
    }
    return $state_acronym;
  }

  //NOT FORM BUT HERE ANYWAY
  public function ncse_get_attachment_id($url){
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $url ));
    return $attachment[0];
  }

}

?>
