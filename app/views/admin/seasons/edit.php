<?php $this->ncse_form->construct($model->name); ?>

<h2>Edit Seasons</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Inactive',$this->form->checkbox_input('inactive',array('label'=>false,'value'=>'1','checked'=>($object->inactive=='1'?true:false))));
?>
</table>

<?php echo $this->form->end('Update'); ?>