<h2><?php echo MvcInflector::pluralize_titleize($model->name); ?></h2>
<?php
// $arr = get_defined_vars();
// print_r($arr);

// print_r($this);
?>
<form id="posts-filter" action="<?php echo MvcRouter::admin_url(); ?>" method="get">
    <div><a href="<?php echo MvcRouter::admin_url(array('controller'=>$params['controller'],'action'=>'add')); ?>" class="button">Add</a></div>
    <p class="search-box">
        <label class="screen-reader-text" for="post-search-input">Search:</label>
        <input type="hidden" name="page" value="<?php echo MvcRouter::admin_page_param($model->name); ?>" />
        <input type="text" name="q" value="<?php echo empty($params['q']) ? '' : $params['q']; ?>" />
        <input type="submit" value="Search" class="button" />
    </p>

</form>

<div class="tablenav">

    <div class="tablenav-pages">

        <?php echo paginate_links($pagination); ?>

    </div>

</div>

<div class="clear"></div>

<table class="widefat post fixed striped" cellspacing="0">

    <thead>
        <?php echo $helper->admin_header_cells($this); ?>
    </thead>

    <tfoot>
        <?php echo $helper->admin_header_cells($this); ?>
    </tfoot>

    <tbody>
        <?php echo $helper->admin_table_cells($this, $objects, array(
          'actions'=>array(
            'edit' => true,
            'view' => false,
            'delete' => true,
          )
        )); ?>
    </tbody>

</table>

<div class="tablenav">

    <div class="tablenav-pages">

        <?php echo paginate_links($pagination); ?>

    </div>

</div>

<br class="clear" />
