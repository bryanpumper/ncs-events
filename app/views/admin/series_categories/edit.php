<?php $this->ncse_form->construct($model->name); ?>

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<script src='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js'></script>

<h2>Edit Series Categories</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Icon',$this->ncse_form->image_uploader('icon_url',$object->icon_url));
  echo $this->ncse_form->table_row_data('Background color',$this->form->input('background_color',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Calendar color',$this->form->input('calendar_color',array('label'=>false)));
  echo $this->ncse_form->wysiwyg_editor('Description',$object->description,'description');
?>
</table>
<?php if(count($object->series)>0): ?>
<div id="series">
  <h2>Series Order</h2>
  <div>
    <ul id="listWithHandle" class="list-group">
    <?php foreach ($object->series as $key => $value) { ?>
      <li class="list-group-item" data-id="<?php echo $value->id; ?>">
        <div class="dragme">
          <i class="fa fa-arrows" aria-hidden="true"></i>
          <?php echo $value->short_name.' - '.$value->name; ?>
        </div>
      </li>
    <?php } ?>
    </ul>
  </div>
</div>
<?php endif; ?>

<?php echo $this->form->end('Update'); ?>

<?php $this->ncse_form->load_image_uploader_script(); ?>

<script src="<?php echo plugins_url('ncs-events/app/lib/js/Sortable.min.js'); ?>"></script>
<script>

  jQuery(document).ready(function($) {

    $("#SeriesCategoryBackgroundColor").spectrum({
      preferredFormat: "hex",
      showInput: true,
    });

    $("#SeriesCategoryCalendarColor").spectrum({
      preferredFormat: "hex",
      showInput: true,
    });

  });

  Sortable.create(listWithHandle, {
    handle: '.dragme',
    animation: 150,
    store: {
      get: function (sortable) {
        var data = {
          action: 'admin_series_get_sort_order',
          nonce: '<?php echo wp_create_nonce( "admin_series_get_sort_order" ); ?>',
          id: <?php echo $object->id; ?>,
        };
        var order = [];
        jQuery.ajax({
          url: '<?php echo admin_url('admin-ajax.php'); ?>',
          async: false,
          data: data,
          method: 'POST',
          success: function(response){
            if (response && response!=='') {
              order = response.split(',')
            }
          },
        });
        jQuery('table').show();
        return order;
    },
    set: function (sortable) {
      var order = sortable.toArray();
      var data = {
        action: 'admin_series_update_sort_order',
        nonce: '<?php echo wp_create_nonce( "admin_series_update_sort_order" ); ?>',
        id: <?php echo $object->id; ?>,
        order: order.join(',')
      };

      jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response){
      });
    }
  }
  });
</script>
