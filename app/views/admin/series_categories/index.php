<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

<h2><?php echo MvcInflector::pluralize_titleize($model->name); ?></h2>
<?php
// $arr = get_defined_vars();
// print_r($arr);

// print_r($this);
?>
<form id="posts-filter" action="<?php echo MvcRouter::admin_url(); ?>" method="get">
    <div><a href="<?php echo MvcRouter::admin_url(array('controller'=>$params['controller'],'action'=>'add')); ?>" class="button">Add</a></div>
    <p class="search-box">
        <label class="screen-reader-text" for="post-search-input">Search:</label>
        <input type="hidden" name="page" value="<?php echo MvcRouter::admin_page_param($model->name); ?>" />
        <input type="text" name="q" value="<?php echo empty($params['q']) ? '' : $params['q']; ?>" />
        <input type="submit" value="Search" class="button" />
    </p>

</form>

<div class="tablenav">

    <div class="tablenav-pages">

        <?php echo paginate_links($pagination); ?>

    </div>

</div>

<div class="clear"></div>

<table class="widefat post fixed striped hidden" cellspacing="0">

    <thead>
      <?php
        $html = '';
        foreach ($this->default_columns as $key => $column) {
            $html .= $helper->admin_header_cell($column['label']);
        }
        $html .= $helper->admin_header_cell('');
        echo '<tr><th style="width:20px;"></th>'.$html.'</tr>';
      ?>
    </thead>

    <tfoot>
      <?php
        echo '<tr><th></th>'.$html.'</tr>';
      ?>
    </tfoot>

    <tbody id="listWithHandle" class="list-group">
      <?php
        $html = '';
        $options = array(
            'actions'=>array(
              'edit' => true,
              'view' => false,
              'delete' => true,
          )
        );
        foreach ($objects as $object) {
            $html .= '<tr class="list-group-item" data-id="'.$object->id.'">';
            $html .= '<td class="dragme"><i class="fa fa-arrows" aria-hidden="true"></i></td>';
            foreach ($this->default_columns as $key => $column) {
                $html .= $helper->admin_table_cell($this, $object, $column, $options);
            }
            $html .= $helper->admin_actions_cell($this, $object, $options);
            $html .= '</tr>';
        }
        echo $html;
      ?>
    </tbody>

</table>

<div class="tablenav">

    <div class="tablenav-pages">

        <?php echo paginate_links($pagination); ?>
    </div>

</div>

<br class="clear" />

<script src="<?php echo plugins_url('ncs-events/app/lib/js/Sortable.min.js'); ?>"></script>
<script>
  Sortable.create(listWithHandle, {
    handle: '.dragme',
    animation: 150,
    store: {
      get: function (sortable) {
        var data = {
          action: 'admin_series_categories_get_sort_order',
          nonce: '<?php echo wp_create_nonce( "admin_series_categories_get_sort_order" ); ?>',
        };
        var order = [];
        jQuery.ajax({
          url: '<?php echo admin_url('admin-ajax.php'); ?>',
          async: false,
          data: data,
          success: function(response){
            if (response && response!=='') {
              order = response.split('|')
            }
          },
        });
        jQuery('table').show();
        return order;
		},
    set: function (sortable) {
			var order = sortable.toArray();
      var data = {
        action: 'admin_series_categories_update_sort_order',
        nonce: '<?php echo wp_create_nonce( "admin_series_categories_update_sort_order" ); ?>',
        order: order.join('|')
      };

      jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response){
      });
		}
  }
  });
</script>
