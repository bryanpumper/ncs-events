<?php $this->ncse_form->construct($model->name); ?>

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css' />
<script src='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js'></script>

<h2>Add Cities</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->table_row_data('State',$this->form->input('state',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Icon',$this->ncse_form->image_uploader('icon_url',$object->icon_url));
  echo $this->ncse_form->table_row_data('Background Color (hex)',$this->form->input('color',array('label'=>false)));
?>
</table>

<?php echo $this->form->end('Add'); ?>

<?php $this->ncse_form->load_image_uploader_script(); ?>

<script type="text/javascript">

jQuery(document).ready(function($) {

    $("#CityColor").spectrum({
      preferredFormat: "hex",
      showInput: true,
    });

});

</script>
