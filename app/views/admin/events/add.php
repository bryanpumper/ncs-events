<?php $this->ncse_form->construct($model->name); ?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css"/ >
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css"/ >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  .remove-artist {
    float: right;
    cursor: pointer;
  }
  .drag {
    margin-right: 5px;
  }
  .artist-container {
    border: 1px solid #ccc;padding: 5px;margin-bottom: 10px;
  }
</style>

<h2>Add Event</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Inactive',$this->form->checkbox_input('availability',array('label'=>false,'value'=>'inactive','checked'=>($object->availability=='inactive'?true:false))));
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->wysiwyg_editor('Short Description',$object->short_description,'short_description');
  echo $this->ncse_form->wysiwyg_editor('Description',$object->description,'description');
  echo $this->ncse_form->wysiwyg_editor('Program',$object->program,'program');
  echo $this->ncse_form->table_row_data('Sponsor',$this->form->belongs_to_dropdown('Sponsor', $sponsors, array('style' => 'width: 200px;', 'empty' => true, 'label' => false)));
  echo $this->ncse_form->table_row_data('Ticket price starts at',$this->form->input('ticket_price_start',array('label'=>false)));
  echo $this->ncse_form->table_row_data('On Sale Date',$this->ncse_form->date_picker('on_sale_date'));
  echo $this->ncse_form->wysiwyg_editor('Sidebar Text',$object->sidebar_text,'sidebar_text');
  echo $this->ncse_form->table_row_data('Header Image',$this->ncse_form->image_uploader('header_image_url',$object->header_image_url));
?>
  <tr>
    <td>
      <label for="image_url">Featured Image</label>
    </td>
    <td>
      <input type="text" name="data[Event][featured_image_url]" class="regular-text image_url">
      <input type="button" name="upload-btn"class="button-secondary upload-btn" value="Upload Image">
    </td>
  </tr>
  <tr>
    <td>
      <label for="image_url">Images</label>
    </td>
    <td>
      <div class="event-images-container" data-num="0">
        <input type="text" name="data[EventImage][0][image_url]" class="regular-text image_url event-images">
        <input type="button" name="upload-btn"class="button-secondary upload-btn" value="Upload Image">
        <span class="remove-data"><a href="#">Remove</a></span>
      </div>
      <button class="add-event-images">Add new image</button>
    </td>
  </tr>
  <?php echo $this->ncse_form->table_row_data('Dont display more options',$this->form->checkbox_input('dont_display_more_options',array('label'=>false,'value'=>1,'checked'=>($object->dont_display_more_options==1?true:false)))); ?>
  <tr><td></td><td>*To not show the view more concerts and view ticket package options button on event page</td></tr>
</table>

<hr/>
<h3>Dates</h3>
<table id="event-dates">
  <thead>
    <td>Date</td>
    <td>Time</td>
    <td>Venue</td>
    <td>Artists</td>
    <td>Ticket Link</td>
    <td></td>
  </thead>
  <tr data-num="0">
    <td class="table-date">
      <input class="datepicker" type="text" name="data[EventDate][0][date]" data-name="date">
    </td>
    <td class="table-time">
      <input class="timepicker" type="text" name="data[EventDate][0][time]" data-name="time">
    </td>
    <td class="table-venue">
      <select class="venuepicker" name="data[EventDate][0][venue_id]" data-name="venue_id">
      <?php echo $this->ncse_form->venue_select2_options($city_and_venue,'') ?>
      </select>
    </td>
    <td class="table-artist">
      <select class="artistspicker" data-input-name="data[EventDate][0][Artist][ids]" data-name="ids">
        <option value="">Select an artist to add</option>
        <?php echo $this->ncse_form->artist_select2_options($artists); ?>
      </select>
      <button class="add-artist">Add</button>
      <div class="artist-container">
        <ul data-id="artists'.$value->id.'" class="artists-list">
        </ul>
        <input type="hidden" class="artist-order" name="data[EventDate][0][artists_order]" value=""/>
      </div>
    </td>
    <td class="table-ticket">
      <input type="text" name="data[EventDate][0][ticket_link]" data-name="ticket_link">
    </td>
    <td class="table-row-remove"><a href="#">Remove</a></td>
  </tr>
</table>
<button class="add-new-date" style="margin-bottom:20px;">Add new date</button>

<hr/>
<div class="note-wrapper">
  <h3>Notes</h3>
  <div class="event-note-container" data-num="0">
    <div class="event-note-selection">
      Note Type
      <span>
        <select class="event-note-type" name="data[EventNote][0][type]" >
          <option value="Additional Event Notes">Additional Event Notes</option>
          <option value="Program Notes">Program Notes</option>
          <option value="Payment Notes">Event Notes</option>
        </select>
      </span>
      <span>
        For Date
        <select id="event-note-date-id" name="data[EventNote][0][event_date_id]">
          <option value="">All</option>
        </select>
      </span>
    </div>
    <?php wp_editor( '', str_replace(' ','_',strtolower('note_description0')), array('media_buttons'=>true,'editor_height'=>'200','textarea_name'=>'data[EventNote][0][description]') ); ?>
    <span class="remove-data"><a href="#">Remove</a></span>
  </div>
  <button class="add-new-note">Add new note</button>
</div>

<div class="media-wrapper">
  <h3>Media</h3>
  <div class="event-media-container" data-num="0">
    <div>
      Media Type
      <span>
        <select class="event-media-type" name="data[EventMedia][0][type]" >
          <option value="podcast">Podcast</option>
          <option value="audio">Audio</option>
          <option value="video">Youtube Video</option>
        </select>
      </span>
    </div>
    <div class="event-media-url-container">
      <span>Url</span>
      <input type="text" name="data[EventMedia][0][media_url]" class="regular-text event-media-url image_url">
      <input type="button" name="upload-btn" class="button-secondary upload-btn" value="Upload Media">
    </div>
    <span class="remove-data"><a href="#">Remove</a></span>
  </div>

  <button class="add-new-media">Add new media</button>
</div>

<div style="margin-top:30px;">
<?php echo $this->form->end('Add'); ?>
</div>

<script src="<?php echo plugins_url('ncs-events/app/lib/js/Sortable.min.js'); ?>"></script>
<script type="text/javascript">
var sortable = []
var maxMediaDataNum = 0
var maxNoteAudioDataNum = 0
jQuery(document).ready(function($) {

  var el = document.getElementById('artist-list');
  $.each($('.artists-list'),function(index,val){
    sortableDataId = $(val).attr('data-id')
    sortable[sortableDataId] = Sortable.create(val,{
      handle: ".drag",
      onEnd: function (/**Event*/evt) {
        var sortableDataId = $(evt.target).attr('data-id')
        $(evt.target).siblings('.artist-order').val(sortable[sortableDataId].toArray())
      },
    });
  })

  $.each($('.artist-order'),function(index,val){
    if ($(val).val()==='') {
      var sortableDataId = $(val).parent().find('.artists-list').attr('data-id')
      $(val).val(sortable[sortableDataId].toArray())
    }
  })

  $('body').on('click','.add-artist',function(e){
    e.preventDefault();
    if ($(this).parent().children('select').val() !== '') {
      var selectedText = $(this).parent().children('select').find('option:selected').text();
      var selectedId = $(this).parent().children('select').val();
      var dataNum = $(this).siblings('.artist-container').find('li').length;
      var inputName = $(this).parent().children('select').attr('data-input-name');
      inputName = inputName + '['+dataNum+']';

      if ($(this).siblings('.artist-container').find('[data-id="'+selectedId+'"]').length < 1) {
        $(this).siblings('.artist-container').find('ul').append('<li data-id="'+selectedId+'"><span class="drag"><i class="fa fa-arrows" aria-hidden="true"></i></span>'+selectedText+'<input type="hidden" name="'+inputName+'" data-num="'+dataNum+'" value="'+selectedId+'"><span class="remove-artist">X</span></li>');
        var sortableDataId = $(this).siblings('.artist-container').find('.artists-list').attr('data-id')
        $(this).siblings('.artist-container').find('.artist-order').val(sortable[sortableDataId].toArray())
      }
      reorderArtistsName($(this).siblings('.artist-container').find('.artists-list'));
    }

  })

  $('body').on('click','.remove-artist',function(e){
    if ($(this).parent().siblings().length > 0) {
      var artistsList = $(this).parents('.artists-list')
      var sortableDataId = $(this).parents('.artists-list').attr('data-id')
      $(this).parents('.artists-list').siblings('.artist-order').val(sortable[sortableDataId].toArray())
      $(this).parent().remove();
      reorderArtistsName(artistsList);
    }else{
      alert('Need to have 1 artist')
    }
  })

  function reorderArtistsName(artistsList){
    var dataName = $(artistsList).parent().siblings('select').attr('data-input-name')
    $.each($(artistsList).find('li input'),function(index,value){
      $(value).attr('data-num',index)
      $(value).attr('name',dataName+'['+index+']')
    })
  }

  function initElementScripts(){

    jQuery('.datepicker').datetimepicker({
      timepicker:false,
      format:'Y-m-d',
      scrollMonth: false,
      scrollTime: false,
      scrollInput: false
    });

    jQuery('.timepicker').datetimepicker({
      datepicker:false,
      format:'H:i',
      step:30
    });

    jQuery('.venuepicker').select2();
    $('.artistspicker').select2();

  }

  initElementScripts();

  jQuery('.add-new-date').click(function(){
    var lastTr = $('#event-dates tr').last().clone();
    var dataNum = parseInt(lastTr.attr('data-num'));
    var newDataNum = dataNum+1;
    lastTr.attr('data-num',newDataNum);

    lastTr.find('.table-id input').remove()
    //DATE
    var dateFieldName = lastTr.find('.table-date input').attr('data-name');
    lastTr.find('.table-date input').attr('name','data[EventDate]['+newDataNum+']['+dateFieldName+']');
    lastTr.find('.table-date input').val('');
    //TIME
    var timeFieldName = lastTr.find('.table-time input').attr('data-name');
    lastTr.find('.table-time input').attr('name','data[EventDate]['+newDataNum+']['+timeFieldName+']');
    lastTr.find('.table-time input').val('');
    //VENUE
    lastTr.find('.table-venue .select2').remove();
    var venueFieldName = lastTr.find('.table-venue select').attr('data-name');
    lastTr.find('.table-venue select').attr('name','data[EventDate]['+newDataNum+']['+venueFieldName+']');
    lastTr.find('.table-venue select').removeClass('select2-hidden-accessible');
    //ARTIST
    lastTr.find('.table-artist .select2').remove();
    var artistFieldName = lastTr.find('.table-artist select').attr('data-name');
    lastTr.find('.table-artist select').attr('data-input-name','data[EventDate]['+newDataNum+'][Artist]['+artistFieldName+']');
    lastTr.find('.table-artist select').removeClass('select2-hidden-accessible');
    lastTr.find('.table-artist .artist-order').attr('name','data[EventDate]['+newDataNum+'][artists_order]')
    lastTr.find('.table-artist .artists-list').attr('data-id','artists'+newDataNum)
    if (lastTr.find('.table-artist .artists-list li input').length>0) {
      $.each(lastTr.find('.table-artist .artists-list li input'),function(index,value){
        var artistDataNum = $(value).attr('data-num')
        $(value).attr('name','data[EventDate]['+newDataNum+'][Artist][ids]['+artistDataNum+']')
      })
    }
    var sortableDataId = 'artists'+newDataNum
    sortable[sortableDataId] = Sortable.create(lastTr.find('.table-artist .artists-list')[0],{
      handle: ".drag",
      onEnd: function (/**Event*/evt) {
        var sortableDataId = $(evt.target).attr('data-id')
        $(evt.target).siblings('.artist-order').val(sortable[sortableDataId].toArray())
      },
    });
    //TICKET LINK
    var ticketLinkFieldName = lastTr.find('.table-ticket input').attr('data-name');
    lastTr.find('.table-ticket input').attr ('name','data[EventDate]['+newDataNum+']['+ticketLinkFieldName+']');
    lastTr.find('.table-ticket input').val('');

    $('#event-dates tr').last().after(lastTr);
    initElementScripts();
    return false;
  })

  $(document).on('click','.table-row-remove a',function(e){
   e.preventDefault();
   if ($('.table-row-remove a').size()>1) {
     $(this).parents('tr').remove();
   }else{
     alert('Add a new one before deleting the last');
   }
  });

  $(document).on('click','.upload-btn',function(e){
    e.preventDefault();
    var that = $(this);
    var image = wp.media({
      title: 'Upload Image',
      multiple: false
    }).open().on('select', function(e){
      var uploaded_image = image.state().get('selection').first();
      console.log(uploaded_image);
      var image_url = uploaded_image.toJSON().url;
      that.parent().find('.image_url').val(image_url)
    });
  })

  // FOR EVENT NOTES AUDIO
  var maxMediaDataNumArray = []
  var maxNoteAudioDataNumArray = []
  $.each($('.event-media-container'),function(index,value){
    var dataNum = $(value).attr('data-num')
    maxMediaDataNumArray.push(parseInt(dataNum))
  })

  $.each($('.event-note-audio-container'),function(index,value){
    var dataNum = $(value).attr('data-num')
    maxNoteAudioDataNumArray.push(parseInt(dataNum))
  })

  if (maxMediaDataNumArray.length > 0) {
    window.maxMediaDataNum = Math.max.apply(Math, maxMediaDataNumArray);
  }

  if (maxNoteAudioDataNumArray.length > 0) {
    window.maxNoteAudioDataNum = Math.max.apply(Math, maxNoteAudioDataNumArray);
  }

  $(document).on('change','.event-note-type',function(e){
    var type = $(this).val();
    if (type === 'Program Notes') {
      var dataNum = window.maxNoteAudioDataNum;
      var newDataNum = dataNum+1;
      window.maxNoteAudioDataNum = newDataNum
      var noteDataNum = $(this).parents('.event-note-container').attr('data-num')
      var htmlOutput = '<div class="event-note-audio-container" style="margin-top:20px;" data-num="'+newDataNum+'">'
        + '<span>Audio Url</span> '
        + '<input class="audio_note_id" type="hidden" name="data[EventNoteAudio]['+newDataNum+'][note_id]" value="'+noteDataNum+'" />'
        + '<input type="text" name="data[EventNoteAudio]['+newDataNum+'][audio_url]" class="regular-text event-media-url image_url">'
        + '<input type="button" name="upload-btn"class="button-secondary upload-btn" value="Upload Audio">'
      + '</div>';
      $(this).parents('.event-note-selection').after(htmlOutput)
    }else{
      $(this).parents('.event-note-container').find('.event-note-audio-container').remove();
    }
  })
  //END OF EVENT NOTES AUDIO

  $('.add-event-images').click(function(e){
    e.preventDefault();
    var lastImage = $('.event-images-container').last();
    var newImage = lastImage.clone();
    var dataNum = parseInt(lastImage.attr('data-num'));
    var newDataNum = dataNum+1;
    newImage.attr('data-num',newDataNum);
    newImage.find('.image_id').remove();
    newImage.find('.event-images').attr ('name','data[EventImage]['+newDataNum+'][image_url]');
    newImage.find('.event-images').val('');
    lastImage.after(newImage);
  });


  $('.add-new-note').click(function(e){
      e.preventDefault();
      var lastNote = $('.event-note-container').last();
      var newNote = lastNote.clone();
      var dataNum = parseInt(lastNote.attr('data-num'));
      var newDataNum = dataNum+1;
      newNote.attr('data-num',newDataNum);
      newNote.find('.wp-core-ui').remove();
      newNote.find('.event-note-type').attr('name','data[EventNote]['+newDataNum+'][type]');
      newNote.find('.event-note-type').val('Additional Event Notes')
      newNote.find('#event-note-date-id').attr('name','data[EventNote]['+newDataNum+'][event_date_id]').val('');
      newNote.find('.event-note-audio-container').remove();

      $.ajax({
         url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
         data: {
           'action': 'ncse_wysiwyg_editor',
           'content_value': '',
           'title': 'note_description'+newDataNum,
           'field_name': 'data[EventNote]['+newDataNum+'][description]',
           'media_buttons': true
         },
         success: function(data) {
           if ($('.event-note-container').size() > 0) {
             newNote.append(data);
             var removeElem = newNote.find('.remove-data');
             newNote.append(removeElem)
            //  $('.event-note-container').last().after(data);
           }else{
             $('.add-new-note').before(data);
           }
           lastNote.after(newNote)
           quicktags({id : 'note_description'+newDataNum});
           tinymce.execCommand( 'mceAddEditor', true, 'note_description'+newDataNum );
          //  tinymce.init(tinyMCEPreInit.mceInit['asasds']);
          //@TODO bug on tinymce when adding new note. switching text to visual will show the bug
         },
         type: 'POST'
      });
    });

    $(document).on('change','.event-media-type',function(e){
      var mediaUrlText = '';
      switch ($(this).val()) {
        case 'podcast':
          mediaUrlText = 'Podcast Url';
          $(this).parents('.event-media-container').find('.upload-btn').show();
          break;
        case 'audio':
          mediaUrlText = 'Audio Url';
          $(this).parents('.event-media-container').find('.upload-btn').show();
          break;
        case 'video':
          mediaUrlText = 'Yotube Video Url';
          $(this).parents('.event-media-container').find('.upload-btn').hide();
          break;
      }
      $(this).parents('.event-media-container').find('.event-media-url-container span').text(mediaUrlText);
    })

    $('.add-new-media').click(function(e){
      e.preventDefault();
      var lastMedia = $('.event-media-container').last();
      var newMedia = lastMedia.clone();
      // var dataNum = parseInt(lastMedia.attr('data-num'));
      var dataNum = window.maxMediaDataNum;
      var newDataNum = dataNum+1;
      newMedia.attr('data-num',newDataNum);
      newMedia.find('.event_media_id').remove();
      newMedia.find('.event-media-type').attr('name','data[EventMedia]['+newDataNum+'][type]');
      newMedia.find('.event-media-type').val('');
      newMedia.find('.event-media-url').attr('name','data[EventMedia]['+newDataNum+'][media_url]');
      newMedia.find('.event-media-url').val('');
      newMedia.find('.event-media-url-container span').text('');
      newMedia.find('.upload-btn').show();
      lastMedia.after(newMedia);
    });

    $(document).on('click','.remove-data',function(e){
      e.preventDefault();
      var parentClass = $(this).parent().attr('class');
      if ($('.'+parentClass).size()>1) {
        $(this).parent().remove();
      }else{
        alert('Add a new one before deleting the last');
      }
    });


});



</script>
