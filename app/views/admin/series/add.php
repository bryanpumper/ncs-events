<?php $this->ncse_form->construct($model->name); ?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css"/ >
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css"/ >
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css' />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<h2>Add Series</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Season',$this->form->belongs_to_dropdown('Season', $seasons, array('label'=>false,'style' => 'width: 200px;', 'empty' => true)));
  echo $this->ncse_form->table_row_data('Series Category',$this->form->belongs_to_dropdown('SeriesCategory', $series_categories, array('label'=>false,'style' => 'width: 200px;', 'empty' => true)));
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false,'style' => 'width: 350px;')));
  echo $this->ncse_form->table_row_data('Short Name',$this->form->input('short_name',array('label'=>false,'style' => 'width: 350px;')));
  echo $this->ncse_form->table_row_data('Number of Concerts',$this->form->input('concert_numbers',array('label'=>false,'style' => 'width: 350px;')));
  echo $this->ncse_form->wysiwyg_editor('Description',$object->description,'description');
  echo $this->ncse_form->table_row_data('Purchase Link',$this->form->input('purchase_link',array('label'=>false,'style' => 'width: 350px;')));
  echo $this->ncse_form->table_row_data('On Sale Date',$this->ncse_form->date_picker('on_sale_date'));
  echo $this->ncse_form->table_row_data('Subscription Pricing Ends',$this->ncse_form->date_picker('pricing_ends_date'));
  echo $this->ncse_form->table_row_data('PDF chart of concerts',$this->ncse_form->image_uploader('chart_of_concerts_url',$object->chart_of_concerts_url,'Upload PDF'));
?>
</table>

<div>
  <h3>Events</h3>
  <table id="events">
    <thead>
      <td>Event</td>
      <td></td>
    </thead>
    <tr data-num="0">
      <td class="table-events">
        <select class="event_dates" name="data[Series][EventDate][ids][]" data-name="event_date">
          <option value="">Select Event</option>
          <?php echo $this->ncse_form->series_event_dates_select_options($events,''); ?>
        </select>
      </td>
      <td class="table-row-remove"><a href="#">Remove</a></td>
    </tr>
  </table>
  <button class="add-new-event" style="margin-bottom:20px;">Add new event</button>
</div>

<?php echo $this->form->end('Add'); ?>

<script type="text/javascript">

jQuery(document).ready(function($) {

    function initElementScripts(){
      $('.event_dates').select2();
    }

    initElementScripts();

    jQuery('.add-new-event').click(function(){
      var lastTr = $('#events tr').last().clone();
      var dataNum = parseInt(lastTr.attr('data-num'));
      var newDataNum = dataNum+1;
      lastTr.attr('data-num',newDataNum);

      // lastTr.find('.table-id input').remove()

      lastTr.find('.table-events .select2').remove();
      var venueFieldName = lastTr.find('.table-events select').attr('data-name');
      lastTr.find('.table-events select').attr('name','data[Series][EventDate][ids][]');
      lastTr.find('.table-events select').removeClass('select2-hidden-accessible');

      $('#events tr').last().after(lastTr);
      initElementScripts();
      return false;
    });

    jQuery('.datepicker').datetimepicker({
      timepicker:false,
      format:'Y-m-d'
    });

    $(document).on('click','.table-row-remove a',function(e){
     e.preventDefault();
     if ($('.table-row-remove a').size()>1) {
       $(this).parents('tr').remove();
     }else{
       alert('Add a new one before deleting the last');
     }
    });

    $('.upload-btn').click(function(e) {
      e.preventDefault();
      var that = $(this);
      var image = wp.media({
        title: 'Upload PDF',
        multiple: false
      }).open().on('select', function(e){
        var uploaded_image = image.state().get('selection').first();
        var image_url = uploaded_image.toJSON().url;
        that.parent().find('.image_url').val(image_url)
      });
    });



});

</script>
