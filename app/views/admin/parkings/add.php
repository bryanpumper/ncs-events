<h2>Add Parkings</h2>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBzBB6kppzMiDOvlfJJqiBNxU7kHn5-Ysg&libraries=places&v=3"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false))); ?>
<tr>
  <td>
    Map
  </td>
  <td>
    <?php echo $this->form->hidden_input('map_latlong',array('label'=>false)); ?>
    <input type="text" class="map" placeholder="Search Address here"/>
  </td>
</tr>
<tr>
  <td></td>
  <td>
    <div class="map-canvas" style="width:500px;height:300px;"></div>
  </td>
</tr>
<?php echo $this->ncse_form->table_row_data('City',$this->form->belongs_to_dropdown('City', $cities, array('label'=>false,'style' => 'width: 200px;', 'empty' => true))); ?>
</table>

<?php echo $this->form->end('Add'); ?>

<script type="text/javascript">

jQuery(document).ready(function($) {
  $('.map').geocomplete({
      map: ".map-canvas",
      mapOptions: {
        scrollwheel: true,
        // zoom: 10
      },
      markerOptions: {
        draggable: true
      },
      // types: ['geocode','parking']
  }).bind("geocode:result", function(event, result){
    var latLng = result.geometry.location;
    var latLong = latLng.lat() + ',' + latLng.lng();
    $('#ParkingMapLatlong').val(latLong);
  }).bind("geocode:dragged", function(event, latLng){
    var latLong = latLng.lat() + ',' + latLng.lng();
    $('#ParkingMapLatlong').val(latLong);
    // console.log('https://www.google.com/maps/place/Meymandi/@'+latLong+',19.5z')
  });;
});

</script>
