<?php $this->ncse_form->construct($model->name); ?>

<h2>Add Artists</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->wysiwyg_editor('Biography',$object->biography,'biography');
?>
</table>

<?php echo $this->form->end('Add'); ?>
