<?php $this->ncse_form->construct($model->name); ?>

<h2>Add Media</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Type',$this->form->select('type',array('label'=>false,'options'=>array('podcast'=>'Podcast','audio'=>'Audio','video'=>'Youtube video'),'value'=>$object->type)));
  echo $this->ncse_form->table_row_data('Media Url',$this->form->input('media_url',array('label'=>false)));
?>
</table>

<?php echo $this->form->end('Add'); ?>

<?php $this->ncse_form->load_image_uploader_script(); ?>
