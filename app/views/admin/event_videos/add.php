<?php $this->ncse_form->construct($model->name); ?>

<h2>Add Video</h2>

<?php echo $this->form->create($model->name); ?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Sponsor Url',$this->form->input('url',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Logo',$this->ncse_form->image_uploader('logo_url',$object->logo_url));
?>
</table>

<?php echo $this->form->end('Add'); ?>

<?php $this->ncse_form->load_image_uploader_script(); ?>
