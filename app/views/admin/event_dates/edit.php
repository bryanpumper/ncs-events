<h2>Edit Event Date</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('Date',$this->form->input('date',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Time',$this->form->input('time',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Ticket Link',$this->form->input('ticket_link',array('label'=>false)));
  echo $this->ncse_form->table_row_data('Event',$this->form->belongs_to_dropdown('Event', $events, array('label'=>false, 'style' => 'width: 200px;', 'empty' => true)));
  echo $this->ncse_form->table_row_data('Venue',$this->form->belongs_to_dropdown('Venue', $venues, array('label'=>false, 'style' => 'width: 200px;', 'empty' => true)));
?>
</table>

<?php echo $this->form->end('Update'); ?>
