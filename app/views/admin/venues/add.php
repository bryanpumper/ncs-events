<?php $this->ncse_form->construct($model->name); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzBB6kppzMiDOvlfJJqiBNxU7kHn5-Ysg&libraries=places&v=3"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>

<h2>Add Venues</h2>

<?php
  echo $this->form->create($model->name);
  echo $this->ncse_form->display_action_buttons($params);
?>

<table>
<?php
  echo $this->ncse_form->table_row_data('City',$this->form->belongs_to_dropdown('City', $cities, array('label'=>false,'style' => 'width: 200px;', 'empty' => true)));
  echo $this->ncse_form->table_row_data('Name',$this->form->input('name',array('label'=>false)));
  echo $this->ncse_form->wysiwyg_editor('Description',$object->description,'description');
  echo $this->ncse_form->wysiwyg_editor('Address',$object->address,'address');
  echo $this->ncse_form->wysiwyg_editor('Contact Numbers',$object->contact_numbers,'contact_numbers');
  echo $this->ncse_form->table_row_data('Image',$this->ncse_form->image_uploader('image_url',$object->image_url));
  echo $this->ncse_form->table_row_data('Header Image',$this->ncse_form->image_uploader('header_image_url',$object->header_image_url));
  echo $this->ncse_form->wysiwyg_editor('Seating Chart',$object->seating_chart,'seating_chart');
  echo $this->ncse_form->table_row_data('Seating Chart Image',$this->ncse_form->image_uploader('seating_chart_url',$object->seating_chart_url));
  echo $this->ncse_form->table_row_data('Map Link',$this->form->input('map_link',array('label'=>false)));
?>
<tr>
  <td>
    Map
  </td>
  <td>
    <?php echo $this->form->hidden_input('map_latlong',array('label'=>false)); ?>
    <input type="text" class="map" placeholder="Search Address here"/>
  </td>
</tr>
<tr>
  <td></td>
  <td>
    <div class="map-canvas" style="width:500px;height:300px;"></div>
  </td>
</tr>
<?php
  echo $this->ncse_form->table_row_data('Box Office Number',$this->form->input('box_office_number',array('label'=>false)));
  echo $this->ncse_form->wysiwyg_editor('Restaurant Discounts',$object->restaurant_discounts,'restaurant_discounts',true,true);
  echo $this->ncse_form->wysiwyg_editor('Accessibility Description',$object->accessibility_description,'accessibility_description');
?>
</table>

<?php echo $this->form->end('Add'); ?>



<script type="text/javascript">
  jQuery(document).ready(function($){

    $('.upload-btn').click(function(e) {
      e.preventDefault();
      var that = $(this);
      var image = wp.media({
        title: 'Upload Image',
        multiple: false
      }).open().on('select', function(e){
        var uploaded_image = image.state().get('selection').first();
        console.log(uploaded_image);
        var image_url = uploaded_image.toJSON().url;
        that.parent().find('.image_url').val(image_url)
      });
    });

    $('.map').geocomplete({
        map: ".map-canvas",
        mapOptions: {
          scrollwheel: true,
          // zoom: 10
        },
        markerOptions: {
          draggable: true
        },
        // types: ['geocode','parking']
    }).bind("geocode:result", function(event, result){
      var latLng = result.geometry.location;
      var latLong = latLng.lat() + ',' + latLng.lng();
      $('#VenueMapLatlong').val(latLong);
    }).bind("geocode:dragged", function(event, latLng){
      var latLong = latLng.lat() + ',' + latLng.lng();
      $('#VenueMapLatlongg').val(latLong);
      // console.log('https://www.google.com/maps/place/Meymandi/@'+latLong+',19.5z')
    });;

  });
</script>
