<div class="blank-div"></div>

<div id="content" class="site-content container">
	<div id="primary">
		<div id="main" class="site-main" role="main">
			<article id="post-377" class="post-377 page type-page status-publish has-post-thumbnail hentry">
				<div class="entry-content">
					<div class="row row-fluid">
						<div class="col-sm-12">
							<div class="custom-row">
								<h2><?php echo $series->series_category->name; ?> <span>SERIES</span></h2>
							</div>
						</div>
					</div>

					<?php
						$series_background_color = $series->background_color;
						if (!$series_background_color) {
							$series_background_color = $series->series_category->background_color;
						}
						$series_bg_style = 'background:'.$series_background_color;
					?>

					<div class="row event-headings">
						<div class="col-sm-8">
							<h3 class="event-subhead">
								<?php 
									if ($series->concert_numbers) {
										echo $series->concert_numbers."-Concert Package";
									}
								?>
							</h3>
							<div style="margin-bottom: 10px;"><?php echo $series->description; ?></div>
						</div>
						<div class="col-sm-4">
							<?php if(!empty($series->pricing_ends_date) && $series->pricing_ends_date >= date('Y-m-d') && !empty($series->purchase_link)): ?>
								<div class="clearfix" style="margin-bottom: 10px;"><a type="button" class="btn btn-default" href="<?php echo $series->purchase_link; ?>">PURCHASE PACKAGE</a></div>
								<div class="ncs-purchase-options">
									<p>To purchase online, you will be prompted to create a North Carolina Symphony Account.</p>
									<div><strong>By Phone:</strong> 919.733.2750</div>
									<div><strong>In Person:</strong> 3700 Glenwood Ave., Raleigh, NC 27612</div>
								</div>		
							<?php endif; ?>
						</div>
					</div>
					<div class="row-fluid se-featured-shows">
						<?php
							if(count($series->event_dates)>0){
								$events = array();
								$event_model = mvc_model('EventDate');
								foreach ($series->event_dates as $key => $value) {
									$event_date = $event_model->find_by_id($value->id, array(
										'joins' => array('Event'),
										'includes' => array('Event'),
									));
									if ($event_date->date < date('Y-m-d') || $event_date->event->availability == 'inactive') {
										continue;
									}
									$event_image = $event_date->event->featured_image_url;
									
									if (empty($event_image)) {
										$event_image = '/wp-content/themes/ncsymphony/images/placeholder-750x374.jpg';
                  					}
									$event_media = '';
									if (count($event_date->event->event_medias)>0) {
										foreach ($event_date->event->event_medias as $mediaKey => $mediaValue) {
											if ($mediaValue->type=='video' && empty($event_video)) {
												$event_media = wp_oembed_get( $mediaValue->media_url, array('width'=>500,'height'=>220) );
											}
										}
									}
									if (empty($event_media)) {
										if (count($event_date->event->event_images)>0) {
											if (!empty($event_date->event->event_images[0]->image_url)) {
												$event_media = '<img src="'.$event_date->event->event_images[0]->image_url.'" width="380" height="220"/>';
											}
										}
									}
									if (array_key_exists($event_date->event_id, $events)) {
										$events[$event_date->event->id]['dates'][] = array('venue'=>$event_date->venue->name.', '.$event_date->venue->city->name,'date_original'=> $event_date->date,'date'=>mysql2date( 'l, F j, Y', $event_date->date ).' '.mysql2date( 'g:i a', $event_date->time ));
									}else{
										$events[$event_date->event->id] = array(
											'name' => $event_date->event->name,
											'image' => $event_image,
											'dates'=> array(array('city'=>$event_date->venue->city->name,'date_original'=> $event_date->date,'date'=>mysql2date( 'l, F j, Y', $event_date->date ).' | '.mysql2date( 'g:i a', $event_date->time ),'ticket_link'=>$event_date->ticket_link,'venue'=>$event_date->venue->name.', '.$event_date->venue->city->name)),
											'event_obj' => $event_date->event,
											'media' => $event_media
										);
									}
							}
							$count = 0;
							end($events);
							$lastEventKey = key($events);
							foreach($events as $key=>$event) {
								$eventObj = $event['event_obj'];
								if ($count % 2 == 0) {
									echo '<div class="custom-dropdown">';
								}
								echo '<div class="dropdown hidden-xs" data-eventkey="'.$eventObj->id.'">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">';
								echo '<div class="home-event-img"><img src="'.$event['image'].'"/></div>';
								echo '<div class="event-info">';
								if (count($event['dates'])>1) {
									echo '<p>'.$event['name'].'</p><br>';
									end($event['dates']);
									$lasteventDate = $event['dates'][key($event['dates'])];
									reset($event['dates']);
									echo '<span>';
									echo ncs_verbose_date_range($event['dates'][0]['date_original'],$lasteventDate['date_original']);
								}else{
									echo '<p>'.$event['name'].'</p><br><span>'.$event['dates'][0]['date'];
								}
								echo ' <br> '.$event['dates'][0]['city'].'</span></div>';
								echo '</a>';
								echo '<div class="dropdown-menu hidden-xs" id="eventdrop_'.$eventObj->id.'">
									<div class="top-tip-left"></div>
									<div class="row pop-up-event-info">
										<div class="col-md-4 col-sm-4 schedule-event">';
								foreach ($event['dates'] as $date_key => $date_value) {
									echo '<div>'.$date_value['venue'].'<br><span>'.$date_value['date'].'</span></div>';
								}

								$event_media = $event['media'];
								echo '</div>
										<div class="'.(!empty($event_media)?'col-md-4 col-sm-4':'col-md-8 col-sm-8').' ticket-event">
											'.wpautop($eventObj->short_description);
											if (!empty($eventObj->event_dates[0]->ticket_link)) {
												echo '<a type="button" class="btn btn-default" href="'.$eventObj->event_dates[0]->ticket_link.'" target="_blank">BUY TICKETS</a>';
											}
									echo	'<a type="button" class="btn btn-default" href="'.mvc_public_url(array('object' => $eventObj)).'">LEARN MORE</a>
										</div>';
								if (!empty($event_media)) {
									echo '<div class="col-md-4 col-sm-4 video-event">
												'.$event_media.'
											</div>';
								}
								echo '</div>
												</div>';
								echo '</div>';
								echo '<div class="dropdown visible-xs">
													<a href="'.mvc_public_url(array('object' => $eventObj)).'">';
								echo '<img src="'.$event['image'].'"/>';
								
								// $value->event_dates = array_values($value->event_dates);
								echo '<div class="event-info">
									<p>'.$event['name'].'</p><br><span>'.$event['dates'][0]['date'].' <br> '.$event['dates'][0]['city'].'</span>
								</div>';
								echo '</a></div>';
								if ($count!=0 && $count % 1 == 0) {
									echo '</div>';
								}elseif ($lastEventKey==$key) {
									echo '</div>';
								}

								$count++;
								if ($count==2) {
									$count=0;
								}
							}
						}
					?>

					</div>
			</div><!-- #main -->
		</div><!-- #primary -->
	</div>
</div>
