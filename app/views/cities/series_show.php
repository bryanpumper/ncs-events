<div class="page-img-banner-big hidden-sm hidden-xs" style="background-image: url('/wp-content/uploads/SeasonTicketsPage.jpg');"></div>
<div class="page-img-banner hidden-md hidden-lg">
	<img width="1560" height="500" src="/wp-content/uploads/SeasonTicketsPage.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="/wp-content/uploads/SeasonTicketsPage.jpg 1560w, /wp-content/uploads/seasontickets-300x96.jpg 300w, /wp-content/uploads/SeasonTicketsPage-768x246.jpg 768w, /wp-content/uploads/SeasonTicketsPage-1024x328.jpg 1024w" sizes="(max-width: 1560px) 100vw, 1560px">
</div>

<div id="content" class="site-content container">
	<div id="primary">
    <div id="main" class="site-main" role="main">
      <article id="post-354" class="post-354 page type-page status-publish has-post-thumbnail hentry">
        <div class="entry-content">
          <div class="row row-fluid">
            <div class="col-md-12">
                <div class="custom-row">
                  <h1>TICKET PACKAGES <span><?php echo $city->name; ?></span></h1>
                </div>
            </div>
          </div>
          	<?php 
          		$count = 0;
          		foreach ($seasons as $key => $value) {
          			$html = '<h2 style="color: #333;">'.$value['name'].'</h2>';
          			$html .= '<div class="panel-group" id="accordion">';
          			if (count($value['series_categories']>0)) {
          				foreach ($value['series_categories'] as $series_cat_key => $series_cat_value) {
          					$html .='<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse-'.$key.$series_cat_key.'"><i class="fa fa-caret-right"></i> '.$series_cat_value['name'].'</a>
								</h4>
							</div>
							<div id="collapse-'.$key.$series_cat_key.'" class="panel-collapse collapse">								
								<div class="panel-body genre-listings-info">
								'.wpautop($series_cat_value['description']);
							$series_category_series = array();
							foreach ($series_cat_value['series'] as $series_key => $series_value) {
								$temp_html = '';
								$events = array();
								$event_model = mvc_model('EventDate');
								foreach ($series_value->event_dates as $date_key => $date_value) {
								  if ($date_value->venue->city->id != $city->id) {
									continue;
								  }
								  $event_date = $event_model->find_by_id($date_value->id, array(
									'joins' => array('Event'),
									'includes' => array('Event'),
								  ));
								  $events[$event_date->event->id] = $event_date->event->name;
								}
								if (count($events)<1) {
								  continue;
								}
								$event_count = count($events);
								if ($event_count>14) {
									$event_count = 14;
								}
								$events_list = implode('<br/>',$events);
								$short_name	= (isset($series_value->short_name) && !empty($series_value->short_name)) ? $series_value->short_name : '';  
								$temp_html = '<a type="button" class="genre-listings-concerts btn btn-default" href="'.mvc_public_url(array('object' => $series_value)).'">
										'.strtoupper($short_name).'
									  </a>';
								$series_category_series[$series_value->id] = $temp_html;	  
							  }
							if (!empty($series_cat_value['series_sorting_order'])) {
								$order = $series_cat_value['series_sorting_order'];
								$order = explode(',', $order);
								foreach ($order as $order_key => $order_value) {
									$html .= $series_category_series[$order_value];
								}
							}else{
								foreach ($series_category_series as $cat_series_key => $cat_series_value) {
									$html .= $cat_series_value;				
								}
							}

							 $html .=	'</div>
							</div>
						</div>';

          				}
          			}
          			if ($count==0) {
          				$html .= '<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse-p1908"><i class="fa fa-caret-right"></i> Choose Your Own</a>
								</h4>
							</div>
							<div id="collapse-p1908" class="panel-collapse collapse">								
								<div class="panel-body genre-listings-info">
								'.wpautop(get_post_meta(1908,'wpcf-short-description',true)).'
								<a type="button" class="genre-listings-concerts btn btn-default" href="'.get_permalink(1908).'">
										Choose Your Own
									  </a>
								</div>
							</div>
						</div>';
          			}
          			$html .= '</div>';
          			echo $html;
          			$count++;
          		}

          		if ($count==0) {
          			echo 'Please check back soon for details about our '.date('Y').'/'.date('y', strtotime('+1 year')).' '.$city->name.' series.';
          		}
          	?>
			</div><!-- .entry-content -->
		</div><!-- #main -->
	</div><!-- #primary -->
</div>
