<link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.min.css" type="text/css" media="all">

<?php
$backgroundImgUrl = '';
$postThumbnail = '';
if(!in_array($current_page,array('parking','restaurants'))){
	echo '<div class="page-img-banner">';
	echo '<div id="map"></div>';
	echo '<iframe class="hidden" width="800" height="600" frameborder="0" style="border:0" allowfullscreen=""></iframe>';
	echo '</div>';
}else {
	if ($current_page=='parking') {
		echo '<div class="page-img-banner">';
		echo '<div id="map"></div>';
		echo '</div>';
	}else{
		$backgroundImgUrl = '/wp-content/uploads/restaurant.jpg';
		$postThumbnail = '<img width="1560" height="500" src="/wp-content/uploads/restaurant.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="/wp-content/uploads/restaurant.jpg 1560w, /wp-content/uploads/restaurant-300x96.jpg 300w, /wp-content/uploads/restaurant-768x246.jpg 768w, /wp-content/uploads/restaurant-1024x328.jpg 1024w" sizes="(max-width: 1560px) 100vw, 1560px">';
	}
}
?>

<?php if(!empty($backgroundImgUrl)): ?>
	<div class="page-img-banner-big hidden-sm hidden-xs" style="background-image: url('<?php echo $backgroundImgUrl; ?>');"></div>
	<div class="page-img-banner hidden-md hidden-lg">
		<?php echo $postThumbnail; ?>
	</div>
<?php endif; ?>

<div id="plan-your-visit-subnav">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<form>
					<div class="form-group">
						<label for="sel-loc-plan">Select location:</label>
						<?php echo do_shortcode('[select_venue_location]'); ?>
					</div>
				</form>
			</div>
			<div class="col-md-7">
				<div class="menu-plan-your-visit-menu-container">
          <ul id="menu-plan-your-visit-menu" class="menu">
            <li id="menu-item-310" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310 <?php echo $current_page=='directions'?'current-menu-item':'' ?>"><a href="<?php echo mvc_public_url(array('object' => $venue)).'directions'; ?>">Directions</a></li>
            <li id="menu-item-309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309 <?php echo $current_page=='parking'?'current-menu-item':'' ?>"><a href="<?php echo mvc_public_url(array('object' => $venue)).'parking'; ?>">Parking</a></li>
            <li id="menu-item-308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308"><a href="https://www.ncsymphony.org/plan-your-visit/accessibility/">Accessibility</a></li>
          </ul>
        </div>
      </div>
		</div>
	</div>
</div>

<?php if ( $current_page=='restaurants' ) : ?>
	<?php /**
		$city_query;
		$cities = array(
			1 => 637
		);
		if (array_key_exists($venue->city->id,$cities)) {
			$city_query = new WP_Query( array('page_id' => $cities[$venue->city->id]) );
		}else{
			$city_query = new WP_Query( array('page_id' => 637) );
		}
	?>
	<?php if ( $city_query->have_posts() ) : ?>
	<div id="content" class="site-content container">
		<div id="primary">
	    <div id="main" class="site-main" role="main">
				<div>
	      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	        <div class="entry-content">
					<?php while ($city_query->have_posts()) : $city_query->the_post(); ?>
						<?php echo ncs_do_shortcodes(get_the_content()); ?> <!-- Queried Post Excerpts -->
					<?php endwhile; //resetting the post loop ?>
					</div>
				</article>
				</div>
			</div>
		</div>
	</div><!-- #post-<?php the_ID(); ?> -->
	<?php
	wp_reset_postdata(); //resetting the post query
	endif; **/
	?>

<?php else : ?>
<div id="content" class="site-content container">
	<div id="primary">
    <div id="main" class="site-main" role="main">
      <article class="page type-page status-publish has-post-thumbnail hentry">
        <div class="entry-content">
          <div class="row row-fluid">
            <div class="col-sm-12">
              <div class="custom-row">
                <h1><?php echo $venue->name; ?><span>: <?php echo $venue->city->name; ?></span></h1>
              </div>
            </div>
          </div>
          <div class="row row-fluid">
            <div class="col-sm-8">
              <?php
                if(!empty($venue->description)){
                  echo '<div>'.wpautop($venue->description).'</div>';
                }

                if(!empty($venue->seating_chart)){
                  echo '<div><p><strong>Seating Charts</strong></p>'.wpautop($venue->seating_chart).'</div>';
                }

        /**      if(!empty($venue->accessibility_description)){
                  echo '<div><p><strong>Accessibility</strong></p>'.wpautop($venue->accessibility_description).'</div>';
                } **/

                if(!empty($venue->address)){
                  echo '<div><p><strong>Address</strong></p><div id="venue_address">'.wpautop($venue->address).'</div></div>';
                }

                if (!empty($venue->contact_numbers)) {
                  echo '<div><p><strong>Contact Information</strong></p>'.wpautop($venue->contact_numbers).'</div>';
                }
              ?>
            </div>
            <div class="col-sm-4">
              <figure class="wpb_wrapper vc_figure">
                <div class="vc_single_image-wrapper vc_box_border_grey">
									<?php
										$attachment_id = $this->ncse_form->ncse_get_attachment_id($venue->seating_chart_url);
										echo wp_get_attachment_image( $attachment_id, 'full' );
									?>
                </div>
              </figure>
            </div>
          </div>

					<?php /**if(!empty($venue->restaurant_discounts)): ?> 
						<div class="row row-fluid">
							<div class="col-sm-12">
								<div class="custom-row">
									<h2>RESTAURANT <span>DISCOUNTS</span></h2>
								</div>
							</div>
							<div class="col-sm-12">
								<?php echo '<div>'.wpautop($venue->restaurant_discounts).'</div>'; ?>
							</div>
						</div>
					<?php endif; **/?>
					<?php if($venue->name == 'Meymandi Concert Hall'): ?> 
						<div class="row row-fluid" id="venue-accoms">
							<div class="col-sm-12">
								<h2 style="border-bottom: 1px solid #f7f7f7; margin-bottom: 0;border-radius: 4px;"><a href="/plan-your-visit/restaurant-discounts/"><i class="fa fa-caret-right"></i> Restaurant Discounts</a></h2>
								<div class="panel-group" id="accordion">
									<div class="panel">
										<div class="panel-heading">
											<h2 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed "><i class="fa fa-caret-right"></i> Preferred Accommodations</a>
											</h2>
										</div>
										<div id="collapse-2" class="panel-collapse collapse" style="height: 0px;">								
											<div class="panel-body">
												<a href="http://www.marriott.com/hotels/maps/travel/rdumc-raleigh-marriott-city-center/"><img width="195" height="145" src="/wp-content/uploads/marriot.png"/></a>
											</div>
										</div>
									</div>			
								</div>
							</div>
						</div>
					<?php endif;?>
					
					<?php
						$conditions =  array(
							'EventDate.venue_id' => $venue->id
						);
						$featured_events = do_shortcode('[featured_events conditions="'.base64_encode(serialize($conditions)).'"]');
					?>
					<?php if(!empty($featured_events)): ?>
          <div class="row row-fluid">
            <div class="col-sm-12">
              <div class="custom-row" style="margin-top: 50px;">
                <h2>EVENTS <span>AT THIS VENUE</span></h2>
              </div>
            </div>
						<div class="col-sm-12">
							<?php	echo $featured_events; ?>
						</div>
          </div>
				<?php endif; ?>				
			</div><!-- .entry-content -->
    </article><!-- #post-## -->
	 </div><!-- #main -->
  </div><!-- #primary -->
</div>

<?php endif; ?>


<style>
   #map {
    height: 600px;
    width: 100%;
   }
</style>
<?php
	$mapLocation = $venue->map_latlong;
	$mapLocation = explode(',',$mapLocation);
	$parkingLocations = array();
	foreach ($venue->city->parkings as $key => $value) {
		$parkingLocation = $value->map_latlong;
		$parkingLocation = explode(',',$parkingLocation);
		$parkingLocations[] = array(
			'title' => esc_attr($value->name),
			'lat' => $parkingLocation[0],
			'long' => $parkingLocation[1]
		);
	}
	$parkingJson = json_encode($parkingLocations);

?>
<script>
	function initializeMap() {

<?php if(!in_array($current_page,array('parking','restaurants'))){ ?>
		var uluru = {lat: <?php echo $mapLocation[0] ?>, lng: <?php echo $mapLocation[1] ?>};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: uluru
		});
		$venueAddress = jQuery('#venue_address').html();
		if ($venueAddress==undefined) {
			$venueAddress = '';
		}
		var contentString = '<div id="content">'+
	    '<div id="siteNotice">'+
	    '</div>'+
	    '<div id="bodyContent">'+
	    '<p style="font-size:18px;"><b><?php echo esc_html($venue->name); ?></b><br/>' + $venueAddress +
			'<b>Enter your starting address here for directions to this location.</b><br/>' +
			'<input id="mapDestination" type="text"/><button id="maps_get_directions">GET DIRECTIONS</button>' +
	    '</div>'+
    	'</div>';
		var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
		var image = '/wp-content/themes/ncsymphony/images/ncs-logo-map.png';
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			icon: image
		});
		infowindow.open(map, marker);
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});

		google.maps.event.addListenerOnce(map, 'tilesloaded', mapLoaded);
		function mapLoaded() {
			jQuery('#maps_get_directions').click(function(){
					jQuery('#map').hide();
					var mapOrigin = '<?php echo $mapLocation[0].','.$mapLocation[1]; ?>';
					var mapDestination = jQuery(this).parent().find('#mapDestination').val();
					jQuery('.page-img-banner iframe').attr('src','https://www.google.com/maps/embed/v1/directions?key=AIzaSyBzBB6kppzMiDOvlfJJqiBNxU7kHn5-Ysg&origin='+mapOrigin+'&destination='+mapDestination+'')
					jQuery('.page-img-banner iframe').removeClass('hidden')
			})
		}
<?php }elseif ($current_page=='parking') { ?>
		// console.log('aaa console1!!!');
		var uluru = {lat: <?php echo $mapLocation[0] ?>, lng: <?php echo $mapLocation[1] ?>};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: uluru,
		});
		var imageIcon = {
			url: '/wp-content/themes/ncsymphony/images/parking-icon.png',
			labelOrigin: new google.maps.Point(20, 35)
		}
		function addMarker(feature) {
			var marker = new MarkerWithLabel({
					position: feature.position,
					map: map,
					title: feature.title,
					labelAnchor: new google.maps.Point(40, -2),
					icon: imageIcon
				});
		 }
		var $json_obj = JSON.parse('<?php echo $parkingJson; ?>');
		var features = [];
		jQuery.each($json_obj,function(key,value){
			var obj = {position: new google.maps.LatLng(value.lat,value.long), title: value.title}
			features.push(obj);
		})
		for (var i = 0, feature; feature = features[i]; i++) {
			 addMarker(feature);
		 }

<?php } ?>
	}
</script>