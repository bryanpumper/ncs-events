<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.0/mediaelementplayer.min.css">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<style>
.social-buttons{
	white-space: nowrap;
	padding-top: 5px;
	padding-bottom: 5px;
	vertical-align: top;
	display: inline-block;
	margin-top: 10px;
}

.social-buttons .fb-like span {
	vertical-align: top!important;
}
</style>

<?php
  $backgroundImgUrl = '';
  $postThumbnail = '';
	if(!empty($event->header_image_url)){
    $backgroundImgUrl = $event->header_image_url;
		$attachment_id = $this->ncse_form->ncse_get_attachment_id($event->header_image_url);
		$postThumbnail = wp_get_attachment_image( $attachment_id, 'post_thumbnail_size' );
	}else{
    $backgroundImgUrl = '/wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg';
		$postThumbnail = '<img width="1560" height="758" src="/wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="/wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg 1560w, /wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg 300w, /wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg 768w, /wp-content/themes/ncsymphony/images/placeholder-1560x758.jpg 1024w" sizes="(max-width: 1560px) 100vw, 1560px">';
	}
?>
<div class="page-img-banner-big hidden-sm hidden-xs" style="background-image: url('<?php echo $backgroundImgUrl; ?>');"></div>
<div class="page-img-banner hidden-md hidden-lg">
	<?php echo $postThumbnail; ?>
</div>
<?php
$additionalEventNotes = array();
$programNotes = array();
$paymentNotes = array();
if (count($event->event_notes)>0) {
	foreach ($event->event_notes as $key => $value) {
		switch ($value->type) {
			case 'Additional Event Notes':
				$additionalEventNotes[] = $value;
				break;
			case 'Program Notes':
				$programNotes[] = $value;
				break;
			case 'Payment Notes':
				$paymentNotes[] = $value;
				break;
		}
	}
}

$imagesHtml = ''; $imagesCarousel = '';$imagesControl = '';
$count = 0;
if (count($event->event_images) > 0) {
	foreach ($event->event_images as $key => $value) {
		if (!empty($value->image_url)) {
			$active = '';
			if ($count==0){
				$active = 'active';
			}

			$imagesControl .= '<li data-target="#image" data-slide-to="'.$count.'" class="'.$active.'"></li>';

		    $imagesCarousel .= '
		 	<div class="item '.$active.'">
		      <img src="'.$value->image_url.'" alt="'.$event->name.'">
		    </div>
		    ';
		    $count++;
		}
	// $imagesHtml .= '<div class="col-xs-6 col-md-6">
 //    <a href="#" class="thumbnail" data-toggle="modal" data-target="#thumbnailModal" data-src="'.$value->image_url.'">
 //    	<img src="'.$value->image_url.'"/>
 //    </a></div>';
	}
	if ($count>0) {
		$imagesHtml = '
			<div class="row images-info">
				<div class="col-md-12">
					<div id="carousel-images" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
					  	'.$imagesControl.'
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner" role="listbox">
					  '.$imagesCarousel.'
					  
					  </div>' .

		/**	uncomment if image carousel w/ controls is preferred  <!-- Controls -->
					  <a class="left carousel-control" href="#carousel-images" role="button" data-slide="prev">
					    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="right carousel-control" href="#carousel-images" role="button" data-slide="next">
					    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>  **/
					'</div>
				</div>
			</div>
		';
	}
	
}

$sidebarTextHtml = '';
if (!empty($event->sidebar_text)) {
	$sidebarTextHtml = '<div class="sidebar-text-wrapper">'.wpautop($event->sidebar_text).'</div>';
}



?>

<div id="content" class="site-content container">
	<div id="primary">
				<div id="main" class="site-main" role="main">
          <article id="post-410" class="post-410 page type-page status-publish has-post-thumbnail hentry">
            <div class="entry-content">
              <div class="row row-fluid">
                <div class="col-sm-12">
                  <div class="custom-row">
                    <h1>CONCERTS<span> &amp; EVENTS</span></h1>
                  </div>
                </div>
              </div>

              <div class="row row-fluid">
                <div class="col-sm-12">
                  <div class="wpb_wrapper">
                    <h2><span style="color: #000000;"><?php echo $event->name; ?></span></h2>
                  </div>
                </div>
              </div>

              <div class="row row-fluid events-tabs">
                <div class="col-sm-12">
                  <div class="row">
                    <ul class="nav nav-tabs">
                      <?php foreach ($event->event_dates as $key => $value) {
                        echo '<li'.(($key==0)?' class="active"':'').'><a data-toggle="tab" href="#menu'.($key+1).'">'.$value->venue->city->name.': '.mysql2date( 'D, M j', $value->date ).', '.mysql2date( 'g:ia', $value->time ).'</a></li>';
                      } ?>
                    </ul>

                    <div class="tab-content">
                    	<?php
                            $moreOptionsHtml = ''; 
                            if ($event->dont_display_more_options!=1) {
                            	$moreOptionsHtml = '<div class="view-contick-btns">
									<a type="button" href="/?position=upcomming_events" class="btn btn-default">VIEW MORE EVENTS</a>
									<a type="button" href="/concerts-events/season-tickets/" class="btn btn-default">VIEW TICKET PACKAGE OPTIONS</a>
								</div>';
                            }
                    	?> 

                      <?php foreach ($event->event_dates as $key => $value) {
												$calendar_title = urlencode($event->name);
												$calendar_start_date = date('Ymd\THis', strtotime("$value->date $value->time"));
												$calendar_details = urlencode(wp_strip_all_tags($event->description));
												$calendar_location = urlencode($value->venue->name.', '.$value->venue->city->name);

                        $programNotesHtml = '';
												$programObject = array();
												$videos = array();
												if (count($event->event_medias)>0) {
												foreach ($event->event_medias as $media_key => $media_value) {
														if (in_array($media_value->type,array('audio','podcast'))) {
															if (!empty($media_value->media_url)) {
																$programObject[] = $media_value;
															}
														}else {
																$videos[] = $media_value;
														}
													}

													$notesHtml = '';
													$dateNotesHtml = '';
													if (count($additionalEventNotes)>0) {
														foreach ($additionalEventNotes as $notes_key => $notes_value) {
															if (empty($notes_value->event_date_id) || $notes_value->event_date_id == $value->id) {
																$notesHtml .= '<br/>';
																$notesHtml .= wpautop( $notes_value->description );
																if ($notes_value->event_date_id == $value->id) {
																	$dateNotesHtml = '<br/>'.wpautop( $notes_value->description );
																}
															}
														}
														if (!empty($dateNotesHtml)) {
															$notesHtml = $dateNotesHtml;
														}
													}

													if (count($programNotes)>0) {
														$programNotesHtml .= '<h3>Program Notes</h3>';
														if(count($programNotes)>0){
                              $programNotesOnly = array();
															foreach ($programNotes as $program_key => $program_value) {
                                if (count($program_value->event_note_audios)>0) {
                                  $audio_object = $program_value->event_note_audios[0];
                                  $attachment_id = $this->ncse_form->ncse_get_attachment_id($audio_object->audio_url);
                                  $programTitle = get_the_title($attachment_id);
                                  $programNoteModalLink = '';
                                  if ($program_value->description) {
                                    $programNoteModalLink = '<a type="button" href="#" class="btn btn-default" data-toggle="modal" data-target="#program-notes-'.$program_value->id.'">READ PROGRAM NOTE</a>';
                                  }
                                  $programNotesHtml .= '<div style="margin-bottom:20px;">
                                    <div><strong>'.$programTitle.'</strong></div>
                                    <div class="row row-fluid">
                                      <div class="col-sm-7">
                                        <audio src="'.$audio_object->audio_url.'" controls class="mejs__player"> </audio>
                                      </div>
                                      <div class="col-sm-5">
                                        '.$programNoteModalLink.'
                                      </div>
                                    </div>
                                  </div>';
                                }else{
                                  $programNotesOnly[$program_key] = $program_value;
                                }
                                if (count($programNotesOnly)>0) {
                                  foreach ($programNotesOnly as $program_key => $program_value) {
                                    $programNotesHtml .= '<div><a type="button" href="#" class="btn btn-default" data-toggle="modal" data-target="#program-notes-'.$program_value->id.'">READ PROGRAM NOTE</a></div>';
                                  }
															  }

                                if (!empty($program_value->description)) {
                                  $programNotesHtml .= '
                                    <div class="modal fade" id="program-notes-'.$program_value->id.'" tabindex="-1" role="dialog" aria-labelledby="program-notes-'.$program_value->id.'-label">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="program-notes-'.$program_value->id.'-label">Program Note</h4>
                                          </div>
                                          <div class="modal-body">
                                            '.wpautop($program_value->description).'
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  ';
                                }


                              }
														}
													}
												}

												$programHtml = '';
												if (count($value->event->program)>0) {
													$programHtml .= '<h3>Program</h3>'
														.wpautop( $value->event->program )
													;
												}
												$artistsHtml = '';
												if (count($value->artists)>0) {
													$artistsHtml .= '<h3>Performers</h3><ul>';
													foreach ($value->artists as $artist_key => $artist_value) {
														if (strpos($artist_value->name,',')) {
															$artistObject = explode(',',$artist_value->name);
															$artistsHtml .= '<li><strong>'.$artistObject[0].'</strong>, '.$artistObject[1].'</li>';
														}else{
															$artistsHtml .= '<li><strong>'.$artist_value->name.'</strong></li>';
														}
													}
													$artistsHtml .= '</ul>';
												}
												$videosHtml = '';
												if (count($videos)>0) {
													foreach ($videos as $video_key => $video_value) {
														$videosHtml.= '<div class="ncs-event-promo">
															'.wp_oembed_get($video_value->media_url,array('width'=>560,'height'=>315)).'
														</div>';
													}
												}

												$socialShareHtml = '
														<div class="social-buttons"><div class="fb-like" data-href="'.mvc_public_url(array('object' => $event)).'" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></div>
														<div class="social-buttons"><a class="twitter-share-button"href="https://twitter.com/intent/tweet?text='.urlencode($event->name).'&url='.urlencode(mvc_public_url(array('object' => $event))).'">Tweet</a></div>
														<div class="social-buttons"><div class="g-plusone" data-annotation="none" data-width="300"></div></div>
												';
                        echo '<div id="menu'.($key+1).'" class="tab-pane fade '.(($key==0)?'in active"':'').'">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-8">
								<div class="visible-xs visible-sm">'.(!empty($value->ticket_link)?'<a href="'.$value->ticket_link.'" style="margin-bottom: 10px;" class="btn btn-default">BUY TICKETS</a>':'').'
									'.(!empty($event->ticket_price_start)?'<p class="event-ticket-price">Tickets start at: <span>$'.$event->ticket_price_start.'</span></p>':'').$sidebarTextHtml.'
																	<div class="add-to-cal">
																		<div class="dropdown">
																			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-calendar"></span></button>
																			<ul class="dropdown-menu">
																				<li>'.$this->html->link('iCalendar', array('controller' => 'events', 'action' => 'download_icalendar','id'=>$event->id)).'</li>
																				<li>'.$this->html->link('Google Calendar','http://www.google.com/calendar/event?action=TEMPLATE&text='.$calendar_title.'&dates='.$calendar_start_date.'/'.$calendar_start_date.'&details='.$calendar_details.'&location='.$calendar_location,array('target'=>'_blank')).'</li>
																			</ul>
																			ADD TO CALENDAR
																		</div>
																	</div><p></p>
									<div class="payment-info">
										'.wpautop( $paymentNotes[0]->description ).'
									</div>
								</div>
								<div class="clearfix">
								  <div class="pull-left">
									<div><a href="'.mvc_public_url(array('object' => $value->venue)).'"><strong>'.$value->venue->name.'</strong></a></div>
									'.wpautop($value->venue->address).'
								  </div>
								  <div class="pull-right"></div>
								</div>'
								.$programHtml
								.$artistsHtml
								.(!empty($value->event->description)?'<h3>About This Performance</h3>'.$value->event->description . ' ':'') 
                                .$notesHtml
								.$programNotesHtml.
													/*			.((count($event->sponsor)>0)?'<h3>Sponsored By</h3><img src="'.$event->sponsor->logo_url.'">':'').	*/
                              '</div>
                              <div class="col-md-4">
								<div class="visible-md visible-lg">'.(!empty($value->ticket_link)?'<a href="'.$value->ticket_link.'" style="margin-bottom: 10px;" class="btn btn-default">BUY TICKETS</a>':'').'
								'.(!empty($event->ticket_price_start)?'<p class="event-ticket-price">Tickets start at: <span>$'.$event->ticket_price_start.'</span></p>':'').$sidebarTextHtml.'
																<div class="add-to-cal">
																	<div class="dropdown">
																		<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-calendar"></span></button>
																		<ul class="dropdown-menu">
																			<li>'.$this->html->link('iCalendar', array('controller' => 'events', 'action' => 'download_icalendar','id'=>$event->id)).'</li>
																			<li>'.$this->html->link('Google Calendar','http://www.google.com/calendar/event?action=TEMPLATE&text='.$calendar_title.'&dates='.$calendar_start_date.'/'.$calendar_start_date.'&details='.$calendar_details.'&location='.$calendar_location,array('target'=>'_blank')).'</li>
																		</ul>
																		ADD TO CALENDAR
																	</div>
																</div><p></p>
                                <div class="payment-info">
																	'.wpautop( $paymentNotes[0]->description ).'
                                </div></div>
                                '.$imagesHtml.'
								'.$moreOptionsHtml.'
								'.$videosHtml.'
								</div>
                            </div>

														<div class="row row-fluid">
															<div class="col-sm-12">
																<div class="custom-row">
																	<h2>PLAN<span> YOUR VISIT</span></h2>
																</div>
															</div>
														</div>
														<div class="row row-fluid">
															<div class="col-sm-12 planning-panels">
																<div id="visit-planning">
																	<div class="visit-planning-grp">
																		<a href="'.mvc_public_url(array('object' => $value->venue)).'directions">
																			<img src="/wp-content/uploads/directions.png" alt="Directions">
																			<p>DIRECTIONS</p>
																		</a>
																		<a href="'.mvc_public_url(array('object' => $value->venue)).'parking">
																			<img src="/wp-content/uploads/parking.png" alt="Parking">
																			<p>PARKING</p>
																		</a>
																		<a href="https://www.ncsymphony.org/plan-your-visit/accessibility/">
																			<img src="/wp-content/uploads/accessibility.png" alt="Accessibility">
																			<p>ACCESSIBILITY</p>
																		</a>
																	</div>
																</div>
															</div>
														</div>
                          </div>
                        </div>';
                      } ?>
                    </div>
                  </div>
                </div>
              </div>

            </div><!-- .entry-content -->
          </article><!-- #post-## -->
        </div><!-- #main -->
      </div><!-- #primary -->
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.0/mediaelement-and-player.min.js"></script>
