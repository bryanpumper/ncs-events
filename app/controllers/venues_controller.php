<?php

class VenuesController extends MvcPublicController {

  public function show(){
    $this->load_helper('NcseForm');
    $current_url = set_url_scheme( $_SERVER['REQUEST_URI'] );
    $current_url = substr($current_url, 1, -1);
    $current_url = explode('/',$current_url);
    $current_page = end($current_url);

    $venue_id = $this->params['id'];
    $venue_object = $this->Venue->find_by_id($venue_id);
    $other_venues = $this->Venue->find(array(
      'conditions' => array(
        'Venue.city_id' => $venue_object->city->id
      )
    ));
    foreach ($other_venues as $key => $value) {
      if ($value->id==$venue_id) {
        unset($other_venues[$key]);
      }
    }
    $this->set('venue',$venue_object);
    $this->set('object', $venue_object);
    $this->set('venue_id',$venue_id);
    $this->set('other_venues',$other_venues);
    $this->set('current_page',$current_page);
  }

}

?>
