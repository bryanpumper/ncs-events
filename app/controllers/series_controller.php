<?php

class SeriesController extends MvcPublicController {

	public function show() {
			$this->load_helper('NcseForm');
    	$series_id = $this->params['id'];
    	$series_object = $this->Series->find_by_id($series_id);
    	$this->set('series',$series_object);
    	$this->set('series_id',$series_id);
			$this->set('object', $series_object);
  }

}

?>
