<?php

class CitiesController extends MvcPublicController {

  public function series_show() {

    $city_id = $this->params['id'];
    $city_object = $this->City->find_by_id($city_id);
    $args = array(
      'joins' => array(
        'Series',
        array('table'=>'nsncs15_ncse_series_event_dates','alias'=>'SeriesEventDate','on'=>'SeriesEventDate.series_id=Series.id'),
        array('table'=>'nsncs15_ncse_event_dates','alias'=>'EventDate','on'=>'SeriesEventDate.event_date_id=EventDate.id'),
        array('table'=>'nsncs15_ncse_venues','alias'=>'Venue','on'=>'Venue.id=EventDate.venue_id'),
        array('table'=>'nsncs15_ncse_seasons','alias'=>'Season','on'=>'Series.season_id=Season.id'),
      ),
      'includes' => array('Series'),
      'conditions' => array(
        'EventDate.date >=' => date('Y-m-d'),
        'Series.pricing_ends_date >=' => date('Y-m-d'),
        'Venue.city_id' => $city_id,
        'Season.inactive !=' => 1
      ),
      // 'limit' => 4,
      'group' => 'SeriesCategory.id',
      // 'group' => 'Series.id,EventDate.event_id',
      // 'order' => 'EventDate.date ASC'
    );
    $this->load_model('SeriesCategory');
    $series_categories = $this->SeriesCategory->find($args);
    $seasons = array();
    $instance = 0;
    $singleInstanceObject;
    foreach ($series_categories as $key => $value) {
      if (count($value->series)>0) {
        $this->load_model('Season');
        foreach ($value->series as $series_key => $series_value) {
          if (property_exists($series_value, 'season_id') && !empty($series_value->season_id)) {
            $season = $this->Season->find_by_id($series_value->season_id);
            if (!$season->inactive) {
              $instance++;
              if ($instance==1) {
                $singleInstanceObject = $series_value;
              }
              $season_year = preg_replace('/\D/', '', $season->name);
              if (!array_key_exists($season_year, $seasons)) {
                $seasons[$season_year] = array('name'=>$season->name,'series_categories'=>array());
              }
              if (array_key_exists($value->id, $seasons[$season_year]['series_categories'])) {
                $seasons[$season_year]['series_categories'][$value->id]['series'][$series_value->id] = $series_value;
              }else{
                $seasons[$season_year]['series_categories'][$value->id]['name'] = $value->name;
                $seasons[$season_year]['series_categories'][$value->id]['description'] = $value->description;
                $seasons[$season_year]['series_categories'][$value->id]['series_sorting_order'] = $value->series_sorting_order;
                $seasons[$season_year]['series_categories'][$value->id]['series'][$series_value->id] = $series_value;
              }
            }
          }
        }
      }
    }
    if ($instance>1) {
      krsort($seasons);
    }elseif ($singleInstanceObject) {
      $url = mvc_public_url(array('object' => $singleInstanceObject));
      $this->redirect($url);
    }

    $this->set('seasons',$seasons);
    $this->set('city',$city_object);
    $this->set('city_id',$city_id);
    $this->set('series_categories', $series_categories);
    $this->set('object', $city_object);
  }


  public function set_wp_title($original_title) {
      $separator = ' | ';
      $controller_name = MvcInflector::titleize($this->name);
      $object_name = null;
      $object = null;
      if ($this->action) {
          if (in_array($this->action,array('show','series_show')) && is_object($this->object)) {
              $object = $this->object;
              if (!empty($this->object->__name)) {
                  $object_name = $this->object->__name;
              }
          }
      }
      if ($this->action=='series_show') {
        $controller_name = 'Series';
      }
      $pieces = array(
          $object_name,
          $controller_name
      );
      $pieces = array_filter($pieces);
      $title = implode($separator, $pieces);
      $title = $title.$separator;
      $title_options = apply_filters('mvc_page_title', array(
          'controller' => $controller_name,
          'action' => $this->action,
          'object_name' => $object_name,
          'object' => $object,
          'title' => $title
      ));
      $title = $title_options['title'];
      return $title;
  }

}

?>
