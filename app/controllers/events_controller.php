<?php

class EventsController extends MvcPublicController {
    //@TODO: make a ajax call to reply data
    public function index() {
      $url = get_permalink(204);
      $this->redirect($url);
    }

    public function show() {
      $this->load_helper('NcseForm');
      $event_id = $this->params['id'];
      $event_object = $this->Event->find_by_id($event_id);
      if (!current_user_can('administrator')) {
        if ($event_object && $event_object->availability=='inactive') {
          global $wp_query;
          $wp_query->set_404();
          status_header( 404 );
          get_template_part( 404 ); exit();
        }
      }
      $this->set('event',$event_object);
      $this->set('event_id',$event_id);
      $this->set('object', $event_object);
    }

    public function download_icalendar(){
      $event_date_id = $this->params['id'];
      // $event_date_object = $this->EventDate->find_by_id($event_date_id);
      $this->load_model('EventDate');
      $event_date_object = $this->EventDate->find_by_id($event_date_id);
      $event_id = $event_date_object->event_id;
      $event_object = $this->Event->find_by_id($event_id);
      $slug = $event_object->name;
      $slug = preg_replace('/[^\w]/', '-', $slug);
      $slug = preg_replace('/[-]+/', '-', $slug);
      $slug = strtolower($slug);

      header('Content-type: text/calendar; charset=utf-8');
      header('Content-Disposition: attachment; filename=ncsymphony-'.$slug.'.ics');
      $ics = new ICS(array(
          'location' => $event_date_object->venue->name.', '.$event_date_object->venue->city->name,
          'description' => wp_strip_all_tags($event_object->description),
          'dtstart' => date('Y-m-d H:i:s', strtotime("$event_date_object->date $event_date_object->time")),
          'dtend' => date('Y-m-d H:i:s', strtotime("$event_date_object->date $event_date_object->time")),
          'summary' => $event_object->name,
          'url' => mvc_public_url(array('object' => $event_object))
      ));
      echo $ics->to_string();
      exit;
    }
}

?>
