<?php

class AdminSeriesController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array('id','name');

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
    $this->set_events();
    $this->load_model('SeriesCategory');
    $series_categories = $this->SeriesCategory->find(array('selects' => array('id', 'name')));
    $this->set('series_categories', $series_categories);
    $this->load_model('Season');
    $seasons = $this->Season->find(array('selects' => array('id', 'name')));
    $this->set('seasons', $seasons);
  }

  public function add() {
    if (!empty($this->params['data']) && !empty($this->params['data']['Series'])) {
      $object = $this->params['data']['Series'];
      if (empty($object['id'])) {
        $this->Series->create($this->params['data']['Series']);
        $id = $this->Series->insert_id;
        $url = MvcRouter::admin_url(array('controller' => $this->name, 'action' => 'edit', 'id' => $id));
        $this->flash('notice', 'Successfully created!');
        $this->redirect($url);
      }
    }
    $this->set_objects();
  }

  public function edit() {
    $this->verify_id_param();
    $id = $this->params['id'];
    // $this->processEventModel($id,'EventDate');
    $this->create_or_save();
    $this->set_object();
  }

  private function set_events(){
    $this->load_model('Event');
    $events = $this->Event->find();
    $this->set('events', $events);
  }

  public function update_sort_order(){
    check_ajax_referer( 'admin_series_update_sort_order', 'nonce' );
    if (!empty($_POST['order'])) {
      $this->load_model('SeriesCategory');
      $order = $_POST['order'];
      $this->SeriesCategory->update($_POST['id'], array('series_sorting_order' => $order));
    }
    wp_die();
  }

  public function get_sort_order(){
    check_ajax_referer( 'admin_series_get_sort_order', 'nonce' );
    $order = '';
    if ($_POST['id']) {
      $this->load_model('SeriesCategory');
      $series_categories = $this->SeriesCategory->find_one_by_id($_POST['id']);
      $order = $series_categories->series_sorting_order;
    }
    if (!empty($order)) {
      echo $order;
    }
    wp_die();
  }

}

?>
