<?php

class AdminVenuesController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array(
    'id',
    'name',
    // 'time' => 'EventDates',
  );

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
    $this->load_model('City');
    $cities = $this->City->find(array('selects' => array('id', 'name')));
    $this->set('cities', $cities);
  }

  public function add() {
    // $this->set_cities();
    $this->set_objects();
    $this->create_or_save();
  }

  public function edit() {
    // $this->set_cities();
    $this->verify_id_param();
    $this->set_object();
    $this->create_or_save();
 }

}

?>
