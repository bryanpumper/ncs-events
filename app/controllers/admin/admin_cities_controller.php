<?php

class AdminCitiesController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array('id','name','state');

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
  }

}

?>
