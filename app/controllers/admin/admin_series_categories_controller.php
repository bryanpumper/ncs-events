<?php

class AdminSeriesCategoriesController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array('id','name');

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
  }

  public function update_sort_order(){
    check_ajax_referer( 'admin_series_categories_update_sort_order', 'nonce' );
    if (!empty($_POST['order'])) {
      $order = $_POST['order'];
      update_option( 'ncs-events_series_cat_sort_order', $order );
      echo $order;
    }
    wp_die();
  }

  public function get_sort_order(){
    check_ajax_referer( 'admin_series_categories_get_sort_order', 'nonce' );
    $order = get_option('ncs-events_series_cat_sort_order');
    if (!empty($order)) {
      echo $order;
    }
    wp_die();
  }
}

?>
