<?php

class AdminEventDatesController extends MvcAdminController {

    var $default_columns = array('id', 'date', 'time', 'event' => 'Event', 'venue' );

    var $before = array('load_dependency');

    public function load_dependency(){
      $this->load_helper('NcseForm');
      $this->set_events();
      $this->set_venues();
    }

    public function index() {
      $this->load_helper('Ncse');
      $this->set_objects();
    }

    public function add() {
        $this->set_objects();
        $this->create_or_save();
    }

    public function edit() {
        $this->verify_id_param();
        $this->set_object();
        $this->create_or_save();
     }

    private function set_venues() {
        $this->load_model('Venue');
        $venues = $this->Venue->find(array('selects' => array('id', 'name', 'city_id')));
        $this->set('venues', $venues);
    }

    private function set_events() {
        $this->load_model('Event');
        $events = $this->Event->find(array('selects' => array('id', 'name')));
        $this->set('events', $events);
    }


}

?>
