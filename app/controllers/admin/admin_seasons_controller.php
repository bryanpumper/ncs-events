<?php

class AdminSeasonsController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array(
    'id',
    'name',
    'inactive' => 'Active'
  );

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
  }

  public function index() {
    $this->load_helper('Ncse');
    $this->set_objects();
  }


}

?>
