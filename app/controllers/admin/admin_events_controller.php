<?php
class AdminEventsController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array(
    'id',
    'name',
    'event_dates' => 'Event Dates',
    'availability' => 'Active'
  );

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
    $this->set_venues();
    $this->set_city_and_venue();
    $this->set_artists();
    $this->set_sponsors();
  }

  public function index() {
    $this->load_helper('Ncse');
    $this->set_objects();
  }

  public function add() {
    $this->load_helper('NcseForm');
    if (!empty($this->params['data']) && !empty($this->params['data']['Event'])) {
      $object = $this->params['data']['Event'];
      if (empty($object['id'])) {
        $this->Event->create($this->params['data']['Event']);
        $id = $this->Event->insert_id;
        $url = MvcRouter::admin_url(array('controller' => $this->name, 'action' => 'edit', 'id' => $id));
        //save other models
        if (!empty($this->params['data']['EventDate'])) {
          foreach ($this->params['data']['EventDate'] as $key => $value) {
            if (!empty($value['date'])) {
              $value['event_id'] = $id;
              $this->load_model('EventDate');
              $this->EventDate->save($value);
            }
          }
        }
        if (!empty($this->params['data']['EventImage'])) {
          foreach ($this->params['data']['EventImage'] as $key => $value) {
            if (!empty($value['image_url'])) {
              $value['event_id'] = $id;
              $this->load_model('EventImage');
              $this->EventImage->save($value);
            }
          }
        }
        $eventNoteAudio = array();
        if (!empty($this->params['data']['EventNote'])) {
          foreach ($this->params['data']['EventNote'] as $key => $value) {
            if (!empty($value['description'])) {
              $value['event_id'] = $id;
              $this->load_model('EventNote');
              $this->EventNote->create($value);
              $eventNoteId = $this->EventNote->insert_id;
              $eventNoteAudio[$key] = $eventNoteId;
            }
          }
        }
        if (!empty($this->params['data']['EventNoteAudio'])) {
          foreach ($this->params['data']['EventNoteAudio'] as $key => $value) {
            if (!empty($value['audio_url'])) {
              $this->load_model('EventNoteAudio');
              if (count($eventNoteAudio)>0) {
                $tempNoteAudioId = $this->params['data']['EventNoteAudio'][$key]['note_id'];
                if (array_key_exists($tempNoteAudioId,$eventNoteAudio)) {
                  $value['note_id'] = $eventNoteAudio[$tempNoteAudioId];
                }
              }
              $this->EventNoteAudio->create($value);
            }
          }
        }
        if (!empty($this->params['data']['EventMedia'])) {
          foreach ($this->params['data']['EventMedia'] as $key => $value) {
            if (!empty($value['media_url'])) {
              $value['event_id'] = $id;
              $this->load_model('EventMedia');
              $this->EventMedia->save($value);
            }
          }
        }
        $this->flash('notice', 'Successfully created!');
        $this->redirect($url);
      }
    }
    $this->set_objects();
  }

  public function edit() {
    $this->verify_id_param();
    $id = $this->params['id'];
    $this->processEventModel($id,'EventDate');
    $this->processEventModel($id,'EventImage');
    $noteAudioObject = $this->processEventModel($id,'EventNote');
    if (!empty($this->params['data']['EventNoteAudio'])) {
      $this->load_model('EventNoteAudio');
      foreach ($this->params['data']['EventNoteAudio'] as $key => $value) {
        if (!empty($value['audio_url'])) {
          if (!array_key_exists('id',$value)) {
            if (count($noteAudioObject)>0) {
              $tempNoteId = $value['note_id'];
              if (array_key_exists($tempNoteId,$noteAudioObject)) {
                $value['note_id'] = $noteAudioObject[$tempNoteId];
              }
            }else{
              $noteId = $value['note_id'];
              $note_audio = $this->EventNoteAudio->find_one_by_note_id($noteId);
              if ($note_audio) {
                $this->EventNoteAudio->delete($note_audio->id);
              }
            }
          }
          $this->EventNoteAudio->save($value);
        }
      }
    }
    $this->processEventModel($id,'EventMedia');
    $this->create_or_save();
    $this->set_object();
  }

  private function processEventModel($event_id,$model_name){
    if (!empty($this->params['data']) && !empty($this->params['data'][$model_name])) {
      $this->load_model($model_name);
      $modelObjects = $this->$model_name->find_by_event_id($event_id);
      $object_ids = array();
      foreach ($modelObjects as $object) {
        array_push($object_ids,$object->id);
      }
      if ($model_name==='EventNote') {
        $noteAudioObject = array();
      }
      foreach ($this->params['data'][$model_name] as $key => $value) {
        if(!array_key_exists('id',$value)){
          $value['event_id'] = $event_id;
        }else{
          if (($key = array_search($value['id'], $object_ids)) !== false) {
            unset($object_ids[$key]);
          }
        }

        if(array_key_exists('id',$value)){
          $this->$model_name->save($value);
        }else{
          $this->$model_name->create($value);
          $id = $this->$model_name->insert_id;
          if ($model_name==='EventNote') {
            $noteAudioObject[$key] = $id;
          }
        }

      }
      if (count($object_ids)>0) {
        foreach ($object_ids as $object_id) {
          $this->$model_name->delete($object_id);
        }
      }
      if ($model_name==='EventNote') {
        return $noteAudioObject;
      }
    }
  }

  private function set_event_dates() {
    $this->load_model('EventDate');
    $event_dates = $this->EventDate->find(array('selects' => array('id', 'date')));
    $this->set('event_dates', $event_dates);
  }

  private function set_venues() {
    $this->load_model('Venue');
    $venues = $this->Venue->find(array('selects' => array('id', 'name', 'city_id')));
    $this->set('venues', $venues);
  }

  private function set_city_and_venue(){
    $this->load_model('Venue');
    $venues = $this->Venue->find(array('selects' => array('id', 'name', 'city_id')));
    $cityWithVenue = array();
    foreach ($venues as $key => $value) {
      if(!array_key_exists($value->city->name,$cityWithVenue)){
        $cityWithVenue[$value->city->name] = array();
      }
      array_push($cityWithVenue[$value->city->name],array('id'=>$value->id,'name'=>$value->name));
    }

    $this->set('city_and_venue', $cityWithVenue);
  }

  private function set_artists(){
    $this->load_model('Artist');
    $artists = $this->Artist->find(array('selects' => array('id', 'name')));
    $this->set('artists', $artists);
  }

  private function set_sponsors() {
    $this->load_model('Sponsor');
    $sponsors = $this->Sponsor->find(array('selects' => array('id', 'name')));
    $this->set('sponsors', $sponsors);
  }


}

?>
