<?php

class AdminParkingsController extends MvcAdminController {

  var $default_searchable_fields = array('name');
  var $default_columns = array(
    'id',
    'name',
    // 'time' => 'EventDates',
  );

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
    $this->set_cities();
  }

  public function add() {
    $this->set_objects();
    $this->create_or_save();
  }

  public function edit() {
    $this->verify_id_param();
    $this->set_object();
    $this->create_or_save();
 }

  private function set_cities() {
    $this->load_model('City');
    $cities = $this->City->find(array('selects' => array('id', 'name')));
    $this->set('cities', $cities);
  }


}

?>
