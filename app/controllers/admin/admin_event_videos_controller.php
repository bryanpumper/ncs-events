<?php

class AdminEventVideosController extends MvcAdminController {

  var $default_searchable_fields = array('id');
  var $default_columns = array('id');

  var $before = array('load_dependency');

  public function load_dependency(){
    $this->load_helper('NcseForm');
  }

}

?>
