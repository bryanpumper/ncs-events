<?php

MvcRouter::public_connect('concerts-events/season-tickets/{:id:[\d]+}.*', array('controller' => 'cities', 'action' => 'series_show'));
MvcRouter::public_connect('concerts-events/season-tickets/series/{:id:[\d]+}.*', array('controller' => 'series', 'action' => 'show'));
MvcRouter::public_connect('plan-your-visit/{:id:[\d]+}.*', array('controller' => 'venues', 'action' => 'show'));
MvcRouter::public_connect('events/{:id:[\d]+}.*', array('controller' => 'events', 'action' => 'show'));
MvcRouter::public_connect('{:controller}', array('action' => 'index'));
MvcRouter::public_connect('{:controller}/{:id:[\d]+}', array('action' => 'show'));
MvcRouter::public_connect('{:controller}/{:action}/{:id:[\d]+}');
MvcRouter::admin_ajax_connect(array('controller' => 'admin_series_categories', 'action' => 'update_sort_order'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_series_categories', 'action' => 'get_sort_order'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_series', 'action' => 'update_sort_order'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_series', 'action' => 'get_sort_order'));

?>
