<?php

MvcConfiguration::set(array(
    'Debug' => false
));

add_action('admin_menu', 'ncse_add_admin_pages', 99);

function ncse_add_admin_pages() {
    remove_menu_page('mvc_events');
    remove_menu_page('mvc_event_dates');
    remove_menu_page('mvc_cities');
    remove_menu_page('mvc_venues');
    remove_menu_page('mvc_parkings');
    remove_menu_page('mvc_artists');
    remove_menu_page('mvc_series');
    remove_menu_page('mvc_series_categories');
    remove_menu_page('mvc_sponsors');
    remove_menu_page('mvc_event_notes');
    remove_menu_page('mvc_event_videos');
    remove_menu_page('mvc_event_images');
    remove_menu_page('mvc_event_medias');
    remove_menu_page('mvc_seasons');
    add_menu_page('Events', 'Events', 'manage_options', 'mvc_events');
    add_submenu_page( 'mvc_events', 'Event Dates', 'Event Dates', 'manage_options', 'mvc_event_dates');
    add_submenu_page( 'mvc_events', 'Cities', 'Cities', 'manage_options', 'mvc_cities');
    add_submenu_page( 'mvc_events', 'Venues', 'Venues', 'manage_options', 'mvc_venues');
    add_submenu_page( 'mvc_events', 'Parkings', 'Parkings', 'manage_options', 'mvc_parkings');
    add_submenu_page( 'mvc_events', 'Artists', 'Artists', 'manage_options', 'mvc_artists');
    add_submenu_page( 'mvc_events', 'Series', 'Series', 'manage_options', 'mvc_series');
    add_submenu_page( 'mvc_events', 'Series Category', 'Series Category', 'manage_options', 'mvc_series_categories');
    add_submenu_page( 'mvc_events', 'Sponsors', 'Sponsors', 'manage_options', 'mvc_sponsors');
    add_submenu_page( 'mvc_events', 'Seasons', 'Seasons', 'manage_options', 'mvc_seasons');
    // add_submenu_page( 'mvc_events', 'Event Media', 'Event Media', 'manage_options', 'mvc_event_medias');
}


add_filter('mvc_page_title', 'public_pages_title');
function public_pages_title($args) {
  if (in_array($args['action'],array('show','series_show'))) {
    $args['title'] .= get_bloginfo('name');
  }
	return $args;
}

?>
