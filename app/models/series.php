<?php

class Series extends MvcModel {

    var $table = '{prefix}ncse_series';
    var $display_field = 'name';
    var $belongs_to = array('SeriesCategory','Season');
    var $per_page = 999;
    var $has_and_belongs_to_many = array(
      'EventDate' => array(
          'join_table' => '{prefix}ncse_series_event_dates',
          'fields' => array('id','venue_id')
      )
    );
    var $includes = array('EventDate');

    function to_url($object) {
      $slug = $object->name;
      $slug = preg_replace('/[^\w]/', '-', $slug);
      $slug = preg_replace('/[-]+/', '-', $slug);
      $slug = strtolower($slug);
      return 'concerts-events/season-tickets/series/'.$object->id.'/'.$slug.'/';
    }
    var $order = 'Series.name ASC ,Series.id ASC';

}

?>
