<?php

class Parking extends MvcModel {

    var $table = '{prefix}ncse_parkings';
    var $display_field = 'name';
    var $belongs_to = array('City');
    var $per_page = 20;
    // var $has_many = array('EventDate');
    // var $includes = array('EventDate');
    // var $has_and_belongs_to_many = array(
    //     'Speaker' => array(
    //         'join_table' => '{prefix}events_speakers',
    //         'fields' => array('id', 'first_name', 'last_name')
    //     )
    // );
}

?>
