<?php

class EventDate extends MvcModel {

    var $table = '{prefix}ncse_event_dates';
    var $display_field = 'name';
    var $per_page = 20;
    var $belongs_to = array('Event','Venue');
    var $includes = array('Venue','Artist','Series');
    var $has_many = array('Series');
    var $has_and_belongs_to_many = array(
        'Artist' => array(
            'join_table' => '{prefix}ncse_event_dates_artists',
            'fields' => array('id', 'name')
        )
    );
    var $order = 'EventDate.date ASC, EventDate.time ASC ,EventDate.id ASC';

}

?>
