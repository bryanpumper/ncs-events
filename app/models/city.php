<?php

class City extends MvcModel {

    // id,name,state,icon_url,color

    var $table = '{prefix}ncse_cities';
    var $display_field = 'name';
    var $per_page = 20;
    var $has_many = array('Venue','Parking');
    // var $includes = array('Venue','Parking');
    var $includes = array('Parking');
    // var $has_and_belongs_to_many = array(
    //     'Speaker' => array(
    //         'join_table' => '{prefix}events_speakers',
    //         'fields' => array('id', 'first_name', 'last_name')
    //     )
    // );
    function to_url($object) {
      $slug = $object->name;
      $slug = preg_replace('/[^\w]/', '-', $slug);
      $slug = preg_replace('/[-]+/', '-', $slug);
      $slug = strtolower($slug);
      return 'concerts-events/season-tickets/'.$object->id.'/'.$slug.'/';
    }
}

?>
