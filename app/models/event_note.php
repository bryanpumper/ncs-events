<?php

class EventNote extends MvcModel {

    var $table = '{prefix}ncse_event_notes';
    var $display_field = 'name';
    var $belongs_to = array('Event');
    var $has_many = array(
      'EventNoteAudio' => array(
        'dependent' => true,
        'foreign_key' => 'note_id'
      ),
    );
    var $includes = array('EventNoteAudio');
}

?>
