<?php

class Season extends MvcModel {

    var $table = '{prefix}ncse_seasons';
    var $display_field = 'name';
    var $per_page = 20;
    var $has_many = array('Series');

}

?>
