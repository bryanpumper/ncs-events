<?php

class EventImage extends MvcModel {

    var $table = '{prefix}ncse_event_images';
    var $display_field = 'image_url';
    var $belongs_to = array('Event');
}

?>
