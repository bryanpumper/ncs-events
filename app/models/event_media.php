<?php

class EventMedia extends MvcModel {

    var $table = '{prefix}ncse_event_media';
    var $display_field = 'media_url';
    var $per_page = 20;
    var $belongs_to = array('Event');
}

?>
