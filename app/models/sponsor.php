<?php

class Sponsor extends MvcModel {

    var $table = '{prefix}ncse_sponsors';
    var $display_field = 'name';
    var $per_page = 20;
    var $has_many = array('Event');
}

?>
