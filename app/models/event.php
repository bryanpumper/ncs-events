<?php

class Event extends MvcModel {

    var $table = '{prefix}ncse_events';
    var $display_field = 'name';
    var $per_page = 20;
    var $belongs_to = array('Sponsor');
    var $has_many = array(
      'EventDate' => array(
        'dependent' => true
      ),
      'EventNote' => array(
        'dependent' => true
      ),
      'EventVideo' => array(
        'dependent' => true
      ),
      'EventImage' => array(
        'dependent' => true
      ),
      'EventMedia' => array(
        'dependent' => true
      )
    );
    var $includes = array('EventDate','EventNote','EventVideo','Eventimage','EventMedia');
    // var $has_and_belongs_to_many = array(
    //     'Speaker' => array(
    //         'join_table' => '{prefix}events_speakers',
    //         'fields' => array('id', 'first_name', 'last_name')
    //     )
    // );
    function to_url($object) {
      $slug = sanitize_title($object->name);
      $slug = preg_replace('/[^\w]/', '-', $slug);
      $slug = preg_replace('/[-]+/', '-', $slug);
      $slug = strtolower($slug);
      return 'events/'.$object->id.'/'.$slug.'/';
    }
}

?>
