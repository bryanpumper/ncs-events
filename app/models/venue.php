<?php

class Venue extends MvcModel {

  // city_id,name,description,address,contact_numbers,image_url,seating_chart,seating_chart_url,map_link,map_latlong,box_office_number,accessibility_description

    var $table = '{prefix}ncse_venues';
    var $display_field = 'name';
    var $per_page = 20;
    var $belongs_to = array('City');
    var $includes = array('City');
    // var $includes = array('EventDate');
    // var $has_many = array('EventDate');
    // var $has_and_belongs_to_many = array(
    //     'Speaker' => array(
    //         'join_table' => '{prefix}events_speakers',
    //         'fields' => array('id', 'first_name', 'last_name')
    //     )
    // );

    function to_url($object) {
      $slug = $object->name;
      $slug = preg_replace('/[^\w]/', '-', $slug);
      $slug = preg_replace('/[-]+/', '-', $slug);
      $slug = strtolower($slug);
      return 'plan-your-visit/'.$object->id.'/'.$slug.'/';
    }
}

?>
