<?php

class SeriesCategory extends MvcModel {

    var $table = '{prefix}ncse_series_categories';
    var $display_field = 'name';
    var $has_many = array('Series');
    var $includes = array('Series');
    var $per_page = 999;

}

?>
