<?php
/*
Plugin Name: NCS Events
Plugin URI: http://ncs.clickdemo.net/
Description: Event plugin for North Carolina Symphony
Author: Jensen Christian Lim / Bryan Urfano / Narrasoft Team
Version: 0.1
Author URI: http://ncs.clickdemo.net/
*/

register_activation_hook(__FILE__, 'ncs_events_activate');
register_deactivation_hook(__FILE__, 'ncs_events_deactivate');

include( plugin_dir_path( __FILE__ ) . 'app/lib/ics.php');

function ncs_events_activate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/ncs_events_loader.php';
    $loader = new NcsEventsLoader();
    $loader->activate();
    $wp_rewrite->flush_rules( true );
}

function ncs_events_deactivate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/ncs_events_loader.php';
    $loader = new NcsEventsLoader();
    $loader->deactivate();
    $wp_rewrite->flush_rules( true );
}

function ncse_custom_scripts($hook) {
    wp_enqueue_media();
    if (strpos($hook, 'mvc')!== false) {
      wp_enqueue_script( 'ncse_custom_js', plugin_dir_url( __FILE__ ) . '/admin_custom.js' );
    }
}
add_action( 'admin_enqueue_scripts', 'ncse_custom_scripts' );

function ncse_display_wysiwyg_editor() {
  $value = $_POST['content_value'];
  $title = $_POST['title'] ;
  $field_name = $_POST['field_name'];
  $media_buttons = false;
  if (isset($_POST['media_buttons']) && $_POST['media_buttons']=='true') {
    $media_buttons = true;
  }
  wp_editor( $value, str_replace(' ','_',strtolower($title)), array('media_buttons'=>$media_buttons,'editor_height'=>'200','textarea_name'=>$field_name) );
	wp_die();
}

add_action( 'wp_ajax_ncse_wysiwyg_editor', 'ncse_display_wysiwyg_editor' );

// add_action('admin_init', 'ncse_admin_init');
// function ncse_admin_init(){
//     // Remove unnecessary menus
//     $menus_to_stay = array(
//         // Client manager
//         'nwcm',
//         // Dashboard
//         'index.php',
//         // Users
//         'users.php'
//     );
//     foreach ($GLOBALS['menu'] as $key => $value) {
//         if (!in_array($value[2], $menus_to_stay)) remove_menu_page($value[2]);
//     }
// }

?>
