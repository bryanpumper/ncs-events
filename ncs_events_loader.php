<?php

class NcsEventsLoader extends MvcPluginLoader {

    var $db_version = '1.0';

    function init() {

        // Include any code here that needs to be called when this class is instantiated

        global $wpdb;

        $table_prefix = 'ncse_';

        $this->tables = array(
            'events' => $wpdb->prefix.$table_prefix.'events',
            'event_dates' => $wpdb->prefix.$table_prefix.'event_dates',
            'cities' => $wpdb->prefix.$table_prefix.'cities',
            'venues' => $wpdb->prefix.$table_prefix.'venues',
            'parkings' => $wpdb->prefix.$table_prefix.'parkings',
            'artists' => $wpdb->prefix.$table_prefix.'artists',
            'event_dates_artists' => $wpdb->prefix.$table_prefix.'event_dates_artists',
            'series' => $wpdb->prefix.$table_prefix.'series',
            'series_event_dates' => $wpdb->prefix.$table_prefix.'series_event_dates',
            'series_categories' => $wpdb->prefix.$table_prefix.'series_categories',
            'sponsors' => $wpdb->prefix.$table_prefix.'sponsors',
            'event_notes' => $wpdb->prefix.$table_prefix.'event_notes',
            // 'event_videos' => $wpdb->prefix.$table_prefix.'event_videos',
            'event_media' => $wpdb->prefix.$table_prefix.'event_media',
            'event_images' => $wpdb->prefix.$table_prefix.'event_images',
        );

    }

    function activate() {

        // This call needs to be made to activate this app within WP MVC

        $this->activate_app(__FILE__);

        // Perform any databases modifications related to plugin activation here, if necessary

        require_once ABSPATH.'wp-admin/includes/upgrade.php';

        add_option('ncs_events_db_version', $this->db_version);

        // $sql = '
        //     CREATE TABLE '.$this->tables['events'].' (
        //       id int(11) NOT NULL auto_increment,
        //       venue_id int(9) default NULL,
        //       description text,
        //       is_public tinyint(1) NOT NULL default 0,
        //       PRIMARY KEY  (id),
        //       KEY venue_id (venue_id)
        //     )';
        $sql = '
            CREATE TABLE '.$this->tables['events'].' (
              id int(11) NOT NULL auto_increment,
              sponsor_id int(11) default NULL,
              name varchar(255) NOT NULL,
              description text,
              featured_image_url varchar(255) default NULL,
              ticket_price_start varchar(255) default NULL,
              on_sale_date date default NULL,
              header_image_url varchar(255) default NULL,
              PRIMARY KEY (id),
              KEY sponsor_id (sponsor_id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['event_dates'].' (
              id int(11) NOT NULL auto_increment,
              event_id int(11) default NULL,
              venue_id int(11) default NULL,
              date date default NULL,
              time time default NULL,
              ticket_link varchar(255) default NULL,
              PRIMARY KEY (id),
              KEY event_id (event_id),
              KEY venue_id (venue_id),
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['cities'].' (
              id int(11) NOT NULL auto_increment,
              name varchar(255) NOT NULL,
              state varchar(255) default NULL,
              icon_url varchar(255) default NULL,
              color varchar(255) default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['venues'].' (
              id int(11) NOT NULL auto_increment,
              city_id int(11) default NULL,
              name varchar(255) NOT NULL,
              description text,
              address text default NULL,
              contact_numbers text default NULL,
              image_url varchar(255) default NULL,
              header_image_url varchar(255) default NULL,
              seating_chart text default NULL,
              seating_chart_url varchar(255) default NULL,
              map_link varchar(255) default NULL,
              map_latlong varchar(255) default NULL,
              box_office_number varchar(255) default NULL,
              accessibility_description text default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['parkings'].' (
              id int(11) NOT NULL auto_increment,
              city_id int(11) default NULL,
              name varchar(255) NOT NULL,
              address varchar(255) default NULL,
              map_latlong varchar(255) default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['event_dates_artists'].' (
              id int(11) NOT NULL auto_increment,
              artist_id int(11) default NULL,
              event_date_id int(11) default NULL,
              PRIMARY KEY (id),
              KEY artist_id (artist_id),
              KEY event_date_id (event_date_id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['artists'].' (
              id int(11) NOT NULL auto_increment,
              name varchar(255) NOT NULL,
              biography text,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['series'].' (
              id int(11) NOT NULL auto_increment,
              series_category_id int(11) default NULL,
              name varchar(255) NOT NULL,
              short_name varchar(255) NULL,
              purchase_link varchar(255) default NULL,
              on_sale_date date default NULL,
              pricing_ends_date date default NULL,
              description text,
              PRIMARY KEY (id),
              KEY series_category_id (series_category_id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['series_event_dates'].' (
              id int(11) NOT NULL auto_increment,
              series_id int(11) default NULL,
              event_date_id int(11) default NULL,
              PRIMARY KEY (id),
              KEY series_id (series_id),
              KEY event_date_id (event_date_id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['series_categories'].' (
              id int(11) NOT NULL auto_increment,
              name varchar(255) NOT NULL,
              icon_url varchar(255) default NULL,
              background_color varchar(255) default NULL,
              calendar_color varchar(255) NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['sponsors'].' (
              id int(11) NOT NULL auto_increment,
              name varchar(255) NOT NULL,
              logo_url varchar(255) default NULL,
              url varchar(255) default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['event_notes'].' (
              id int(11) NOT NULL auto_increment,
              event_id int(11) default NULL,
              event_date_id int(11) default NULL,
              type varchar(255) default NULL,
              description text,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        // $sql = '
        //     CREATE TABLE '.$this->tables['event_videos'].' (
        //       id int(11) NOT NULL auto_increment,
        //       event_id int(11) default NULL,
        //       video_url varchar(255) default NULL,
        //       PRIMARY KEY (id)
        //     )';
        // dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['event_media'].' (
              id int(11) NOT NULL auto_increment,
              event_id int(11) default NULL,
              type varchar(255) default NULL,
              media_url varchar(255) default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);

        $sql = '
            CREATE TABLE '.$this->tables['event_images'].' (
              id int(11) NOT NULL auto_increment,
              event_id int(11) default NULL,
              image_url varchar(255) default NULL,
              PRIMARY KEY (id)
            )';
        dbDelta($sql);


        $this->insert_example_data();

    }

    function deactivate() {

        // This call needs to be made to deactivate this app within WP MVC

        $this->deactivate_app(__FILE__);

        // Perform any databases modifications related to plugin deactivation here, if necessary
        // global $wpdb;
        //
        // $sql = 'DROP TABLE '.$this->tables['events'].','.$this->tables['event_dates'];
        // $wpdb->query($sql);
    }

    function insert_example_data() {

      // Only insert the example data if no data already exists
      $site_url = get_site_url();

      $sql = '
          SELECT
              id
          FROM
              '.$this->tables['events'].'
          LIMIT
              1';
      $data_exists = $this->wpdb->get_var($sql);
      if ($data_exists) {
          return false;
      }

      // Insert example data
      $rows = array(
          array(
              'id' => 1,
              'name' => 'Raleigh',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/guitar-icon.png',
              'color' => '#0e499f'
          ),
          array(
              'id' => 2,
              'name' => 'Chapel Hill',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/pantheon-icon.png',
              'color' => '#00aeee'
          ),
          array(
              'id' => 3,
              'name' => 'New Bern',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/sail-icon.png',
              'color' => '#00aaae'
          ),
          array(
              'id' => 4,
              'name' => 'Southern Pines',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/clock-icon.png',
              'color' => '#ca0073'
          ),
          array(
              'id' => 5,
              'name' => 'Wilmington',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/carnival-icon.png',
              'color' => '#8b69b1'
          ),
          array(
              'id' => 6,
              'name' => 'Fayetteville',
              'state' => 'North Carolina',
              'icon_url' => '/wp-content/uploads/star-icon.png',
              'color' => '#51b079'
          ),
          array(
              'id' => 7,
              'name' => 'Cary',
              'state' => 'North Carolina',
              'icon_url' => '',
              'color' => ''
          ),
      );

      foreach($rows as $row) {
          $this->wpdb->insert($this->tables['cities'], $row);
      }

      $rows = array(
          array(
              'id' => 1,
              'city_id' => 1,
              'name' => 'Wake County Parking Deck',
              'map_latlong' => '35.775540, -78.642545',
          ),
      );

      foreach($rows as $row) {
          $this->wpdb->insert($this->tables['parkings'], $row);
      }

      $rows = array(
          array(
              'id' => 1,
              'city_id' => 1,
              'name' => 'Meymandi Concert Hall',
              'image_url' => $site_url.'/wp-content/uploads/meymand-concert-hall.jpg'
          ),
          array(
              'id' => 2,
              'city_id' => 1,
              'name' => 'CAM'
          ),
          array(
              'id' => 3,
              'city_id' => 1,
              'name' => 'Kings'
          ),
          array(
              'id' => 4,
              'city_id' => 1,
              'name' => 'Irregardless'
          ),
          array(
              'id' => 5,
              'city_id' => 1,
              'name' => 'Humble Pie'
          ),
          array(
              'id' => 6,
              'city_id' => 2,
              'name' => 'Kenan Recital Hall'
          ),
          array(
              'id' => 7,
              'city_id' => 2,
              'name' => 'Memorial Hall'
          ),
          array(
              'id' => 8,
              'city_id' => 3,
              'name' => 'New Bern Riverfront Convention Center'
          ),
          array(
              'id' => 9,
              'city_id' => 4,
              'name' => 'Wilson Center'
          ),
          array(
              'id' => 10,
              'city_id' => 5,
              'name' => 'Huff Concert Hall'
          ),
          array(
              'id' => 11,
              'city_id' => 5,
              'name' => 'Seabrook Auditorium'
          ),
          array(
              'id' => 12,
              'city_id' => 6,
              'name' => 'Lee Auditorium'
          ),
          array(
              'id' => 13,
              'city_id' => 7,
              'name' => 'Booth Amphitheatre'
          ),
      );

      foreach($rows as $row) {
          $this->wpdb->insert($this->tables['venues'], $row);
      }

      $rows = array(
          array(
              'id' => 1,
              'name' => 'North Carolina Symphony'
          ),
          array(
              'id' => 2,
              'name' => 'David Glover, conductor'
          ),
          array(
              'id' => 3,
              'name' => 'Grant Llewellyn, conductor'
          ),
          array(
              'id' => 4,
              'name' => 'Jinjoo Cho, violin'
          ),
          array(
              'id' => 5,
              'name' => 'Caroline Shaw, violin'
          ),
      );

      foreach($rows as $row) {
          $this->wpdb->insert($this->tables['artists'], $row);
      }

      $rows = array(
          array(
              'id' => 1,
              'name' => 'Classical'
          ),
          array(
              'id' => 2,
              'name' => 'POPS'
          ),
          array(
              'id' => 3,
              'name' => 'Young Peoples Concerts'
          ),
          array(
              'id' => 4,
              'name' => 'Friday Favorites'
          ),
          array(
              'id' => 5,
              'name' => 'Soundbites at the Pub'
          ),
          array(
              'id' => 6,
              'name' => 'MyMix'
          ),
      );

      foreach($rows as $row) {
          $this->wpdb->insert($this->tables['series'], $row);
      }


    }

}

?>
